part of 'widgets.dart';

class ButtonOutline extends StatelessWidget {
  final Color? textColor;
  final String label;
  final double? width;
  final bool? isLoadingBtn;
  final Function? onPressed;
  const ButtonOutline(
      {super.key,
      this.textColor = Colors.red,
      required this.label,
      this.width,
      this.isLoadingBtn = false,
      this.onPressed});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        height: 45,
        width: width ?? MediaQuery.of(context).size.width,
        child: OutlinedButton(
            onPressed: (isLoadingBtn!)
                ? () {}
                : () {
                    if (onPressed != null) {
                      onPressed!();
                    }
                  },
            style: ElevatedButton.styleFrom(
                shape: RoundedRectangleBorder(
                    side: BorderSide(color: textColor!),
                    borderRadius: BorderRadius.circular(5)),
                foregroundColor: textColor),
            child: (isLoadingBtn!)
                ? SizedBox(
                    height: 20,
                    width: 20,
                    child: CircularProgressIndicator(
                        strokeWidth: 2, color: textColor))
                : Text(label, style: mediumFontStyle)));
  }
}
