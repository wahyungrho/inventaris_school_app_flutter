part of 'widgets.dart';

class TextFieldBox extends StatelessWidget {
  final TextEditingController? controller;
  final String? title;
  final String? hintText;
  final bool? secureText;
  final Widget? suffixIcon;
  final TextInputType? keyboardType;
  final List<TextInputFormatter>? inputFormatter;
  final bool? readOnly;
  final int? maxLines;
  final bool? enabled;
  final bool? autoFocus;
  final TextCapitalization textCapitalization;
  final Function(String)? onChanged;
  const TextFieldBox({
    Key? key,
    this.title,
    this.hintText,
    this.controller,
    this.secureText = false,
    this.suffixIcon,
    this.keyboardType,
    this.inputFormatter,
    this.readOnly = false,
    this.maxLines = 1,
    this.enabled = true,
    this.onChanged,
    this.autoFocus = false,
    this.textCapitalization = TextCapitalization.none,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        (title == null)
            ? const SizedBox()
            : Text(
                title!,
                style: mediumFontStyle,
              ),
        (title == null)
            ? const SizedBox()
            : const SizedBox(
                height: 8,
              ),
        TextField(
          controller: controller,
          obscureText: secureText!,
          keyboardType: keyboardType,
          inputFormatters: inputFormatter,
          readOnly: readOnly!,
          maxLines: maxLines,
          textCapitalization: textCapitalization,
          enabled: enabled!,
          autofocus: autoFocus!,
          onChanged: onChanged,
          decoration: InputDecoration(
              suffixIcon: suffixIcon,
              hintText: hintText ?? "",
              border: InputBorder.none,
              filled: readOnly,
              enabledBorder:
                  const OutlineInputBorder(borderSide: BorderSide(width: 1)),
              focusedBorder:
                  const OutlineInputBorder(borderSide: BorderSide(width: 2)),
              disabledBorder:
                  const OutlineInputBorder(borderSide: BorderSide(width: 1))),
        ),
      ],
    );
  }
}
