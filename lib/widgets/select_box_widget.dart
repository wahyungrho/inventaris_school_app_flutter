part of 'widgets.dart';

class SelectBox extends StatelessWidget {
  final String label;
  final String valueItem;
  final List listItems;
  final Function onChange;
  final String errVal;
  final bool isReadOnly;
  final String? hintText;
  final TextEditingController? searchController;
  final Widget? searchInnerWidget;
  final bool? searchMatchFn;
  final Function(bool)? onMenuStateChange;
  const SelectBox({
    Key? key,
    required this.label,
    required this.valueItem,
    required this.listItems,
    required this.onChange,
    this.errVal = "",
    this.isReadOnly = false,
    this.searchController,
    this.searchInnerWidget,
    this.searchMatchFn,
    this.onMenuStateChange,
    this.hintText,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(label, style: mediumFontStyle),
        const SizedBox(height: 5),
        DropdownButtonHideUnderline(
          child: DropdownButton2(
            hint: Text(hintText ?? label),
            isExpanded: true,
            searchController: searchController,
            searchInnerWidget: searchInnerWidget,
            searchMatchFn: (searchMatchFn != true)
                ? null
                : (item, searchValue) => item.value
                    .toString()
                    .toLowerCase()
                    .contains(searchValue.toLowerCase()),
            onMenuStateChange: onMenuStateChange,
            style: mediumFontStyle.copyWith(
                fontStyle: FontStyle.italic,
                fontSize: 15,
                // color: (isReadOnly == true) ? Colors.black : null),
                color: Colors.black),
            dropdownMaxHeight: MediaQuery.of(context).size.height * 0.5,
            isDense: true,
            // decoration: InputDecoration(
            //   isDense: true,
            //   filled: isReadOnly,
            //   fillColor: (isReadOnly == true) ? Colors.grey.shade200 : null,
            //   focusedBorder: OutlineInputBorder(
            //     borderRadius: BorderRadius.circular(5),
            //     borderSide: BorderSide(
            //       color: kBlackColor.withOpacity(0.5),
            //       width: 1.0,
            //     ),
            //   ),
            //   enabledBorder: OutlineInputBorder(
            //     borderRadius: BorderRadius.circular(5),
            //     borderSide: BorderSide(
            //       color: Colors.grey.shade300,
            //       width: 1.0,
            //     ),
            //   ),
            //   contentPadding:
            //       const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
            // ),
            buttonHeight: kMinInteractiveDimension,
            buttonPadding: const EdgeInsets.fromLTRB(0, 10, 10, 10),
            buttonDecoration: BoxDecoration(
                color: (isReadOnly) ? Colors.grey.shade200 : null,
                borderRadius: BorderRadius.circular(5),
                border: Border.all(width: 1.0)),
            // itemHeight: 30,
            value: valueItem,
            items: listItems.map((item) {
              return DropdownMenuItem(
                value: item['value'].toString(),
                child: Text(item['label'].toString()),
              );
            }).toList(),
            onChanged: (isReadOnly == true)
                ? null
                : (value) async {
                    onChange(value);
                  },
          ),
        ),
        if (errVal != "")
          const SizedBox(
            height: 6,
          ),
        if (errVal != "")
          Padding(
              padding: const EdgeInsets.symmetric(horizontal: 0),
              child: Text(errVal.capitalizeFirstofEach,
                  style: regulerFontStyle.copyWith(
                      fontSize: 13, color: redColor))),
      ],
    );
  }
}
