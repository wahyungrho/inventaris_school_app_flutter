part of 'widgets.dart';

class ButtonPrimary extends StatefulWidget {
  final double? width;
  final bool? isLoadingBtn;
  final Function? onPressed;
  final String label;
  const ButtonPrimary(
      {Key? key,
      this.width,
      this.isLoadingBtn = false,
      this.onPressed,
      required this.label})
      : super(key: key);

  @override
  State<ButtonPrimary> createState() => _ButtonPrimaryState();
}

class _ButtonPrimaryState extends State<ButtonPrimary> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: widget.width ?? MediaQuery.of(context).size.width,
        height: 45,
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
              backgroundColor: primaryColor,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5)),
              foregroundColor: whiteColor),
          onPressed: (widget.isLoadingBtn!)
              ? () {}
              : () {
                  if (widget.onPressed != null) {
                    widget.onPressed!();
                  }
                },
          child: (widget.isLoadingBtn!)
              ? const SizedBox(
                  height: 20,
                  width: 20,
                  child: CircularProgressIndicator(
                    strokeWidth: 2,
                    color: Colors.white,
                  ),
                )
              : Text(widget.label, style: mediumFontStyle),
        ));
  }
}
