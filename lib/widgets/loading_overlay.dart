part of 'widgets.dart';

class LoadingOverlay extends StatelessWidget {
  final bool isLoading;
  final Widget child;
  const LoadingOverlay({Key? key, required this.isLoading, required this.child})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      child,
      if (isLoading)
        Opacity(
            opacity: 0.8,
            child: ModalBarrier(
                dismissible: false, color: greyColor?.withOpacity(0.3))),
      if (isLoading) const Center(child: CircularProgressIndicator())
    ]);
  }
}
