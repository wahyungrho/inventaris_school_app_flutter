import 'package:flutter/material.dart';
import 'package:inventaris_school_app/helpers/models.dart';
import 'package:inventaris_school_app/helpers/shared.dart';
import 'package:inventaris_school_app/helpers/theme.dart';
import 'package:lottie/lottie.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
        child: MaterialApp(
            debugShowCheckedModeBanner: false,
            title: 'Inventaris Sekolah',
            theme: ThemeData(
                colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
                useMaterial3: true),
            home: const SplashScreen()));
  }
}

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  Future<void> getPreference() async {
    SharedPreferences preferences = await GlobalService.sharedPreference;
    bool? isLogin = preferences.getBool(PreferenceModel.isLogin) ?? false;
    if (!mounted) return;
    if (isLogin) {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (_) => const MainPage()));
    } else {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (_) => const SignInPage()));
    }
  }

  @override
  void initState() {
    Future.delayed(const Duration(milliseconds: 3000), () async {
      await getPreference();
    }).onError((error, stackTrace) {
      debugPrint("TERJADI KESALAHAN $error");
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SizedBox(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child:
                Column(mainAxisAlignment: MainAxisAlignment.center, children: [
              Lottie.asset('assets/images/splashscreen.json',
                  height: MediaQuery.of(context).size.width * 0.8),
              const SizedBox(height: 10),
              Text("Inventaris Sekolah",
                  style: boldFontStyle.copyWith(fontSize: 18)),
              const SizedBox(height: 5),
              Text("Manajemen Peralatan dan Perlengkapan Sekolah",
                  style: regulerFontStyle)
            ])));
  }
}
