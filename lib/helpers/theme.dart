import 'package:flutter/material.dart';

const String alertWarning = 'Peringatan !';
const String alertSuccess = 'Berhasil ...';

double defaultMargin = 20.0;

Color? primaryColor = Colors.blue[700];
Color? whiteColor = Colors.white;
Color? redColor = Colors.red;
Color? greyColor = Colors.grey;

TextStyle boldFontStyle = const TextStyle(fontWeight: FontWeight.w700);
TextStyle mediumFontStyle = const TextStyle(fontWeight: FontWeight.w500);
TextStyle regulerFontStyle = const TextStyle(fontWeight: FontWeight.w400);
TextStyle lightFontStyle = const TextStyle(fontWeight: FontWeight.w300);
