part of 'shared.dart';

class Helpers {
  static showDialogAlert(String title, String content, BuildContext context) {
    return showDialog(
        context: context,
        builder: (ctx) => AlertDialog(
                title: Text(
                  title,
                  style: boldFontStyle,
                ),
                content: Text(
                  content,
                  style: regulerFontStyle.copyWith(height: 1.5),
                ),
                actions: <Widget>[
                  TextButton(
                      onPressed: () {
                        Navigator.of(ctx).pop();
                      },
                      child: Text("Tutup", style: boldFontStyle))
                ]));
  }

  static showDialogAlertFunction(
      String title, String content, Function function, BuildContext context,
      {String? textButton}) {
    return showDialog(
        context: context,
        builder: (ctx) => AlertDialog(
                title: Text(
                  title,
                  style: boldFontStyle,
                ),
                content: Text(
                  content,
                  style: regulerFontStyle.copyWith(height: 1.5),
                ),
                actions: <Widget>[
                  TextButton(
                      onPressed: () {
                        function() ?? () {};
                      },
                      child: Text(textButton ?? 'Tutup', style: boldFontStyle))
                ]));
  }

  static showDialogConfirmationFunction(
      String title, String content, Function function, BuildContext context) {
    return showDialog(
        context: context,
        builder: (ctx) => AlertDialog(
                title: Text(
                  title,
                  style: boldFontStyle,
                ),
                content: Text(
                  content,
                  style: regulerFontStyle.copyWith(height: 1.5),
                ),
                actions: <Widget>[
                  TextButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text('Tidak',
                          style:
                              regulerFontStyle.copyWith(color: Colors.grey))),
                  TextButton(
                      onPressed: () {
                        Navigator.pop(context);
                        function() ?? () {};
                      },
                      child: Text('Ya', style: boldFontStyle))
                ]));
  }

  static removeSpecialChar(String text) {
    String result = text.replaceAll(RegExp('[^A-Za-z0-9 ]'), '');
    return result;
  }

  static showSnackBar(BuildContext context, message) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        dismissDirection: DismissDirection.down,
        backgroundColor: Colors.blue,
        content: Text(
          message,
          style: mediumFontStyle,
        )));
  }
}

extension CapExtension on String {
  // fungsi untuk membuat huruf besar untuk huruf pertama dalam string
  String get inCaps =>
      length > 0 ? '${this[0].toUpperCase()}${substring(1)}' : '';

  // fungsi untuk membuat huruf besar dalam string
  String get allInCaps => toUpperCase();

  // fungsi untuk mengganti huruf kapital disetiap kata dalam string
  String get capitalizeFirstofEach => replaceAll(RegExp(' +'), ' ')
      .split(" ")
      .map((str) => str.inCaps)
      .join(" ");
}
