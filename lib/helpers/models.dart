class GlobalModel {
  final String? status;
  final String? timestamp;
  final dynamic errorMessage;
  final dynamic result;

  GlobalModel({this.status, this.timestamp, this.errorMessage, this.result});

  factory GlobalModel.fromJson(Map<String, dynamic> json) {
    return GlobalModel(
      status: json['status'],
      timestamp: json['timestamp'],
      errorMessage: json['error_message'],
      result: json['result'],
    );
  }
}

class PreferenceModel {
  static String userID = 'user_id';
  static String isLogin = 'is_login';
}

class UserModel {
  final String? id;
  final String? nip;
  final String? name;
  final String? phone;
  final String? email;
  final String? status;
  final String? createdAt;
  final String? roleID;
  final String? roleName;
  final String? schoolID;
  final String? schoolName;

  UserModel(
      {this.id,
      this.nip,
      this.name,
      this.phone,
      this.email,
      this.status,
      this.createdAt,
      this.roleID,
      this.roleName,
      this.schoolID,
      this.schoolName});

  factory UserModel.fromJson(Map<String, dynamic> json) => UserModel(
      id: json['id'],
      nip: json['nip'],
      name: json['name'],
      phone: json['phone'],
      email: json['email'],
      status: json['status'],
      createdAt: json['createdAt'],
      roleID: json['roleID'],
      roleName: json['roleName'],
      schoolID: json['schoolID'],
      schoolName: json['schoolName']);
}

class CategoryModel {
  CategoryModel({this.id, this.name, this.status, this.createdAt});

  final String? id;
  final String? name;
  final String? status;
  final String? createdAt;

  factory CategoryModel.fromJson(Map<String, dynamic> json) => CategoryModel(
      id: json['id'],
      name: json['name'],
      status: json['status'],
      createdAt: json['createdAt']);
}

class ProductModel {
  final String? id;
  final String? categoryID;
  final String? categoryName;
  final String? codeProduct;
  final String? name;
  final String? description;
  final String? image;
  final String? stock;
  final String? createdAt;

  ProductModel(
      {this.id,
      this.categoryID,
      this.categoryName,
      this.codeProduct,
      this.name,
      this.description,
      this.image,
      this.stock,
      this.createdAt});

  factory ProductModel.fromJson(Map<String, dynamic> json) => ProductModel(
      id: json['id'],
      categoryID: json['categoryID'],
      categoryName: json['categoryName'] ?? '',
      codeProduct: json['codeProduct'],
      name: json['name'],
      description: json['description'],
      image: json['image'],
      stock: json['stock'],
      createdAt: json['createdAt']);
}

class HistoryLoanModel {
  final String? orderID;
  final String? nip;
  final String? name;
  final String? phone;
  final String? email;
  final String? schoolName;
  final String? roleName;
  final String? codeProduct;
  final String? productName;
  final String? image;
  final String? description;
  final String? stock;
  final String? categoryName;
  final String? quantity;
  final String? notes;
  final String? createLoan;
  final String? statusName;

  HistoryLoanModel(
      {this.orderID,
      this.nip,
      this.name,
      this.phone,
      this.email,
      this.schoolName,
      this.roleName,
      this.codeProduct,
      this.productName,
      this.image,
      this.description,
      this.stock,
      this.categoryName,
      this.quantity,
      this.notes,
      this.createLoan,
      this.statusName});

  factory HistoryLoanModel.fromJson(Map<String, dynamic> data) =>
      HistoryLoanModel(
          orderID: data['orderID'],
          nip: data['nip'],
          name: data['name'],
          phone: data['phone'],
          email: data['email'],
          schoolName: data['schoolName'],
          roleName: data['roleName'],
          codeProduct: data['codeProduct'],
          productName: data['productName'],
          image: data['image'],
          description: data['description'],
          stock: data['stock'],
          categoryName: data['categoryName'],
          quantity: data['quantity'],
          notes: data['notes'],
          createLoan: data['createLoan'],
          statusName: data['statusName']);
}

class ReportModel {
  int? productAll;
  int? productBorrow;
  int? productExisting;
  List<ReportDataProduct>? reportDataProduct;
  List<ReportUsersBorrow>? reportUsersBorrow;

  ReportModel(
      {this.productAll,
      this.productBorrow,
      this.productExisting,
      this.reportDataProduct,
      this.reportUsersBorrow});

  ReportModel.fromJson(Map<String, dynamic> json) {
    productAll = json['product_all'];
    productBorrow = json['product_borrow'];
    productExisting = json['product_existing'];
    if (json['report_data_product'] != null) {
      reportDataProduct = <ReportDataProduct>[];
      json['report_data_product'].forEach((v) {
        reportDataProduct!.add(ReportDataProduct.fromJson(v));
      });
    }
    if (json['report_users_borrow'] != null) {
      reportUsersBorrow = <ReportUsersBorrow>[];
      json['report_users_borrow'].forEach((v) {
        reportUsersBorrow!.add(ReportUsersBorrow.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['product_all'] = productAll;
    data['product_borrow'] = productBorrow;
    data['product_existing'] = productExisting;
    if (reportDataProduct != null) {
      data['report_data_product'] =
          reportDataProduct!.map((v) => v.toJson()).toList();
    }
    if (reportUsersBorrow != null) {
      data['report_users_borrow'] =
          reportUsersBorrow!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ReportDataProduct {
  String? productId;
  String? orderId;
  String? codeProduct;
  String? name;
  String? total;

  ReportDataProduct(
      {this.productId, this.orderId, this.codeProduct, this.name, this.total});

  ReportDataProduct.fromJson(Map<String, dynamic> json) {
    productId = json['product_id'];
    orderId = json['order_id'];
    codeProduct = json['codeProduct'];
    name = json['name'];
    total = json['total'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['product_id'] = productId;
    data['order_id'] = orderId;
    data['codeProduct'] = codeProduct;
    data['name'] = name;
    data['total'] = total;
    return data;
  }
}

class ReportUsersBorrow {
  String? userId;
  String? orderId;
  String? name;
  String? schoolName;
  String? total;

  ReportUsersBorrow(
      {this.userId, this.orderId, this.name, this.schoolName, this.total});

  ReportUsersBorrow.fromJson(Map<String, dynamic> json) {
    userId = json['user_id'];
    orderId = json['order_id'];
    name = json['name'];
    schoolName = json['schoolName'];
    total = json['total'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['user_id'] = userId;
    data['order_id'] = orderId;
    data['name'] = name;
    data['schoolName'] = schoolName;
    data['total'] = total;
    return data;
  }
}

class ListReportProductBorrow {
  String? productId;
  String? orderId;
  String? codeProduct;
  String? name;
  String? total;

  ListReportProductBorrow(
      {this.productId, this.orderId, this.codeProduct, this.name, this.total});

  ListReportProductBorrow.fromJson(Map<String, dynamic> json) {
    productId = json['product_id'];
    orderId = json['order_id'];
    codeProduct = json['codeProduct'];
    name = json['name'];
    total = json['total'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['product_id'] = productId;
    data['order_id'] = orderId;
    data['codeProduct'] = codeProduct;
    data['name'] = name;
    data['total'] = total;
    return data;
  }
}

class ListReportUserBorrow {
  String? userId;
  String? orderId;
  String? name;
  String? schoolName;
  String? total;

  ListReportUserBorrow(
      {this.userId, this.orderId, this.name, this.schoolName, this.total});

  ListReportUserBorrow.fromJson(Map<String, dynamic> json) {
    userId = json['user_id'];
    orderId = json['order_id'];
    name = json['name'];
    schoolName = json['schoolName'];
    total = json['total'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['user_id'] = userId;
    data['order_id'] = orderId;
    data['name'] = name;
    data['schoolName'] = schoolName;
    data['total'] = total;
    return data;
  }
}

class DetailReportUserBorrow {
  String? id;
  String? nip;
  String? name;
  String? phone;
  String? email;
  String? status;
  String? createdAt;
  String? roleID;
  String? roleName;
  String? schoolID;
  String? schoolName;
  List<DataProduct>? dataProduct;

  DetailReportUserBorrow(
      {this.id,
      this.nip,
      this.name,
      this.phone,
      this.email,
      this.status,
      this.createdAt,
      this.roleID,
      this.roleName,
      this.schoolID,
      this.schoolName,
      this.dataProduct});

  DetailReportUserBorrow.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nip = json['nip'];
    name = json['name'];
    phone = json['phone'];
    email = json['email'];
    status = json['status'];
    createdAt = json['createdAt'];
    roleID = json['roleID'];
    roleName = json['roleName'];
    schoolID = json['schoolID'];
    schoolName = json['schoolName'];
    if (json['data_product'] != null) {
      dataProduct = <DataProduct>[];
      json['data_product'].forEach((v) {
        dataProduct!.add(DataProduct.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['nip'] = nip;
    data['name'] = name;
    data['phone'] = phone;
    data['email'] = email;
    data['status'] = status;
    data['createdAt'] = createdAt;
    data['roleID'] = roleID;
    data['roleName'] = roleName;
    data['schoolID'] = schoolID;
    data['schoolName'] = schoolName;
    if (dataProduct != null) {
      data['data_product'] = dataProduct!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class DataProduct {
  String? orderID;
  String? userID;
  String? quantity;
  String? productID;
  String? codeProduct;
  String? productName;
  String? image;
  String? categoryName;
  String? status;

  DataProduct(
      {this.orderID,
      this.userID,
      this.quantity,
      this.productID,
      this.codeProduct,
      this.productName,
      this.image,
      this.categoryName,
      this.status});

  DataProduct.fromJson(Map<String, dynamic> json) {
    orderID = json['orderID'];
    userID = json['userID'];
    quantity = json['quantity'];
    productID = json['productID'];
    codeProduct = json['codeProduct'];
    productName = json['productName'];
    image = json['image'];
    categoryName = json['categoryName'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['orderID'] = orderID;
    data['userID'] = userID;
    data['quantity'] = quantity;
    data['productID'] = productID;
    data['codeProduct'] = codeProduct;
    data['productName'] = productName;
    data['image'] = image;
    data['categoryName'] = categoryName;
    data['status'] = status;
    return data;
  }
}
