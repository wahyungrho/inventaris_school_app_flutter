part of 'shared.dart';

class BaseURL {
  static String domain = 'https://wahyungrho.my.id/api_inventaris_school';
  // static String domain = 'http://192.168.1.12/api_inventaris_school';
  // static String domain = 'http://192.168.1.12/api_inventaris_school_2';
  static String apiURL = '$domain/api';
  static String apiAssets = '$domain/assets';
  static String signin = '$apiURL/auth/signin.php';
  static String signup = '$apiURL/auth/signup.php';
  static String forgotPassword = '$apiURL/auth/forgot_password.php';
  static String resetPassword = '$apiURL/auth/reset_password.php';
  static String userDetail = '$apiURL/user/user_detail.php?id=';
  static String userUpdate = '$apiURL/user/update.php';
  static String schools = '$apiURL/school/schools.php';
  static String categories = '$apiURL/category/categories.php';
  static String formCategories = '$apiURL/category/form.php';
  static String deleteCategories = '$apiURL/category/delete.php';
  static String products = '$apiURL/product/products.php?category=';
  static String addProduct = '$apiURL/product/add.php';
  static String updateProduct = '$apiURL/product/update.php';
  static String deleteProduct = '$apiURL/product/delete.php';
  static String detailProduct = '$apiURL/product/detail.php?id=';
  static String loanInventaris = '$apiURL/loaning/loaning_goods.php';
  static String listLoanInventaris = '$apiURL/loaning/loanings.php';
  static String updateLoanInventaris = '$apiURL/loaning/update_loan.php';
  static String status = '$apiURL/status_loan/status.php';
  static String reports = '$apiURL/reports/product_reports.php';
  static String reportListProduct = '$apiURL/reports/report_product_list.php';
  static String reportListUserBorrow =
      '$apiURL/reports/report_users_borrow_list.php';
  static String reportDetailUserBorrow =
      '$apiURL/reports/report_borrow_by_user.php?id=';
}

class GlobalService {
  static Future<SharedPreferences> sharedPreference =
      SharedPreferences.getInstance();

  static Future<GlobalModel> postData(
      String url, Map<String, dynamic> body, BuildContext context,
      {Map<String, String>? headers}) async {
    try {
      debugPrint(url);
      debugPrint(body.toString());

      http.Response response =
          await http.post(Uri.parse(url), body: body, headers: headers);
      GlobalModel responseModel =
          GlobalModel.fromJson(jsonDecode(response.body));
      debugPrint(response.body);
      return responseModel;
    } on SocketException catch (err) {
      return GlobalModel.fromJson(
          {"status": "error", "error_message": err.osError?.message});
    } catch (e) {
      return GlobalModel.fromJson(
          {"status": "error", "error_message": e.toString()});
    }
  }

  static Future<GlobalModel> getData(String url, BuildContext context) async {
    try {
      debugPrint(url);

      http.Response response = await http.get(Uri.parse(url));
      GlobalModel responseModel =
          GlobalModel.fromJson(jsonDecode(response.body));

      debugPrint(response.body);

      return responseModel;
    } on SocketException catch (err) {
      return GlobalModel.fromJson(
          {"status": "error", "error_message": err.osError?.message});
    } catch (e) {
      return GlobalModel.fromJson(
          {"status": "error", "error_message": e.toString()});
    }
  }

  static void logout(BuildContext context) async {
    SharedPreferences preferences = await sharedPreference;

    preferences.clear();

    // ignore: use_build_context_synchronously
    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (_) => const SignInPage()),
        (route) => false);

    return;
  }
}
