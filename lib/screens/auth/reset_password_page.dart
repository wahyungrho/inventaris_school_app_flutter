part of '../../helpers/shared.dart';

class ResetPasswordPage extends StatefulWidget {
  final String phone;
  final String email;
  const ResetPasswordPage(
      {super.key, required this.phone, required this.email});

  @override
  State<ResetPasswordPage> createState() => _ResetPasswordPageState();
}

class _ResetPasswordPageState extends State<ResetPasswordPage> {
  bool isLoadingButton = false,
      isSecureTextOld = true,
      isSecureTextNew = true,
      isSecureTextConfirm = true;
  final TextEditingController oldPasswordController = TextEditingController();
  final TextEditingController newPasswordController = TextEditingController();
  final TextEditingController confirmPasswordController =
      TextEditingController();

  void validation() {
    if (oldPasswordController.text.isEmpty ||
        newPasswordController.text.isEmpty ||
        confirmPasswordController.text.isEmpty) {
      Helpers.showDialogAlert(
          alertWarning, "Mohon lengkapi form kata sandi baru ...", context);
    } else if (oldPasswordController.text.length < 8 ||
        newPasswordController.text.length < 8 ||
        confirmPasswordController.text.length < 8) {
      Helpers.showDialogAlert(
          alertWarning,
          "Password lama, password baru dan konfirmasi password min 8 karakter ...",
          context);
    } else if (newPasswordController.text != confirmPasswordController.text) {
      Helpers.showDialogAlert(alertWarning,
          "Password tidak sama dengan konfirmasi password ...", context);
    } else {
      submit();
    }
  }

  Future<void> submit() async {
    setState(() {
      isLoadingButton = true;
    });
    Map<String, dynamic> body = {
      'old_password': oldPasswordController.text,
      'new_password': newPasswordController.text,
      'confirm_password': confirmPasswordController.text,
      'email': widget.email,
      'phone': widget.phone
    };
    GlobalModel response =
        await GlobalService.postData(BaseURL.resetPassword, body, context);
    if (!mounted) return;
    if (response.status == 'success') {
      Helpers.showDialogAlertFunction(alertSuccess, response.result, () {
        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (_) => const SignInPage()),
            (route) => false);
      }, textButton: 'Login Sekarang', context);
    } else {
      Helpers.showDialogAlert(alertWarning, response.errorMessage, context);
    }
    setState(() {
      isLoadingButton = false;
    });
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  void dispose() {
    oldPasswordController.dispose();
    newPasswordController.dispose();
    confirmPasswordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text('Kata Sandi Baru')),
        body: ListView(padding: EdgeInsets.all(defaultMargin), children: [
          Container(
              margin: const EdgeInsets.only(bottom: 15),
              child: TextFieldBox(
                  controller: oldPasswordController,
                  title: "Kata Sandi Lama",
                  secureText: isSecureTextOld,
                  suffixIcon: IconButton(
                      onPressed: () {
                        setState(() {
                          isSecureTextOld = !isSecureTextOld;
                        });
                      },
                      icon: Icon(
                        (isSecureTextOld)
                            ? Icons.visibility_off
                            : Icons.visibility,
                        color: Colors.black87,
                      )),
                  hintText: "Masukkan kata sandi lama")),
          Container(
              margin: const EdgeInsets.only(bottom: 15),
              child: TextFieldBox(
                  controller: newPasswordController,
                  title: "Kata Sandi Baru",
                  secureText: isSecureTextNew,
                  suffixIcon: IconButton(
                      onPressed: () {
                        setState(() {
                          isSecureTextNew = !isSecureTextNew;
                        });
                      },
                      icon: Icon(
                        (isSecureTextNew)
                            ? Icons.visibility_off
                            : Icons.visibility,
                        color: Colors.black87,
                      )),
                  hintText: "Masukkan kata sandi baru")),
          Container(
              margin: const EdgeInsets.only(bottom: 15),
              child: TextFieldBox(
                  controller: confirmPasswordController,
                  title: "Konfirmasi Kata Sandi Baru",
                  secureText: isSecureTextConfirm,
                  suffixIcon: IconButton(
                      onPressed: () {
                        setState(() {
                          isSecureTextConfirm = !isSecureTextConfirm;
                        });
                      },
                      icon: Icon(
                        (isSecureTextConfirm)
                            ? Icons.visibility_off
                            : Icons.visibility,
                        color: Colors.black87,
                      )),
                  hintText: "Masukkan konfirmasi kata sandi baru")),
          ButtonPrimary(
              label: 'SUBMIT',
              onPressed: () {
                validation();
              },
              isLoadingBtn: isLoadingButton)
        ]));
  }
}
