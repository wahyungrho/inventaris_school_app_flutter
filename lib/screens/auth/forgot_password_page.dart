part of '../../helpers/shared.dart';

class ForgotPasswordPage extends StatefulWidget {
  const ForgotPasswordPage({super.key});

  @override
  State<ForgotPasswordPage> createState() => _ForgotPasswordPageState();
}

class _ForgotPasswordPageState extends State<ForgotPasswordPage> {
  final TextEditingController emailController = TextEditingController();
  final TextEditingController phoneController = TextEditingController();
  bool isLoadingButton = false;

  void validation() {
    if (phoneController.text.isEmpty || emailController.text.isEmpty) {
      Helpers.showDialogAlert(alertWarning, "Mohon lengkapi form ...", context);
    } else if (!EmailValidator.validate(emailController.text)) {
      Helpers.showDialogAlert(
          alertWarning, "Maaf, Alamat email tidak valid ...", context);
    } else if (phoneController.text.length < 10) {
      Helpers.showDialogAlert(
          alertWarning, "Nomor handphone min 10 angka ...", context);
    } else {
      submit();
    }
  }

  Future<void> submit() async {
    setState(() {
      isLoadingButton = true;
    });
    Map<String, dynamic> body = {
      'email': emailController.text,
      'phone': phoneController.text
    };
    GlobalModel response =
        await GlobalService.postData(BaseURL.forgotPassword, body, context);
    if (!mounted) return;
    if (response.status == 'success') {
      Helpers.showDialogAlertFunction(alertSuccess, response.result, () {
        Navigator.pop(context);
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (_) => ResetPasswordPage(
                    email: body['email'], phone: body['phone'])));
      }, context);
    } else {
      Helpers.showDialogAlert(alertWarning, response.errorMessage, context);
    }
    setState(() {
      isLoadingButton = false;
    });
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  void dispose() {
    emailController.dispose();
    phoneController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text('Lupa Kata Sandi')),
        body: ListView(padding: EdgeInsets.all(defaultMargin), children: [
          Container(
              margin: const EdgeInsets.only(bottom: 15),
              child: TextFieldBox(
                  controller: phoneController,
                  title: 'Nomor Handphone',
                  hintText: 'Masukkan nomor handphone',
                  keyboardType: TextInputType.number,
                  inputFormatter: [FilteringTextInputFormatter.digitsOnly])),
          Container(
              margin: const EdgeInsets.only(bottom: 15),
              child: TextFieldBox(
                  controller: emailController,
                  title: 'Alamat Email',
                  hintText: 'Masukkan alamat email',
                  keyboardType: TextInputType.emailAddress)),
          ButtonPrimary(
              label: 'SUBMIT',
              onPressed: () {
                validation();
              },
              isLoadingBtn: isLoadingButton)
        ]));
  }
}
