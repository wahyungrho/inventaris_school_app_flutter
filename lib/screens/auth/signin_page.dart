part of '../../helpers/shared.dart';

class SignInPage extends StatefulWidget {
  final bool? isCartPage;
  const SignInPage({Key? key, this.isCartPage = false}) : super(key: key);

  @override
  State<SignInPage> createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  final TextEditingController usernameController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  bool isSecureText = true, isLoadingBtn = false;

  @override
  void dispose() {
    usernameController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  void validation() {
    if (usernameController.text.isEmpty || passwordController.text.isEmpty) {
      Helpers.showDialogAlert(
          alertWarning, "Mohon lengkapi form login ...", context);
    } else if (passwordController.text.length < 8) {
      Helpers.showDialogAlert(alertWarning,
          "Password dan konfirmasi password min 8 karakter ...", context);
    } else {
      login();
    }
  }

  Future<void> login() async {
    setState(() {
      isLoadingBtn = true;
    });
    Map<String, dynamic> body = {
      "username": usernameController.text,
      "password": passwordController.text
    };
    GlobalModel response =
        await GlobalService.postData(BaseURL.signin, body, context);
    if (response.status == 'success') {
      SharedPreferences preferences = await GlobalService.sharedPreference;
      preferences.setBool(PreferenceModel.isLogin, true);
      preferences.setString(PreferenceModel.userID, response.result['id']);
      if (!mounted) return;
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (_) => const MainPage()),
          (route) => false);
    } else {
      if (!mounted) return;
      Helpers.showDialogAlert(alertWarning, response.errorMessage, context);
    }
    setState(() {
      isLoadingBtn = false;
    });
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    Widget headerWidget() {
      return Column(children: [
        Image.asset('assets/images/logo.png', height: 100, width: 100),
        const SizedBox(height: 10),
        Text("Inventaris Sekolah", style: boldFontStyle.copyWith(fontSize: 18))
      ]);
    }

    Widget loginText() {
      return Padding(
          padding: EdgeInsets.symmetric(horizontal: defaultMargin),
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Text("Login", style: mediumFontStyle.copyWith(fontSize: 18)),
            const SizedBox(height: 5),
            Text(
                "Silahkan masuk ke dalam aplikasi agar dapat mengatur inventaris Peralatan dan Perlengkapan Sekolah",
                style: regulerFontStyle.copyWith(height: 1.5))
          ]));
    }

    Widget usernameField() {
      return Padding(
          padding: EdgeInsets.symmetric(horizontal: defaultMargin),
          child: TextFieldBox(
              controller: usernameController,
              title: "Nomor Handphone atau Email",
              hintText: "Masukkan nomor handphone atau email"));
    }

    Widget passwordField() {
      return Padding(
          padding: EdgeInsets.symmetric(horizontal: defaultMargin),
          child: TextFieldBox(
              controller: passwordController,
              title: "Kata Sandi",
              secureText: isSecureText,
              suffixIcon: IconButton(
                  onPressed: () {
                    setState(() {
                      isSecureText = !isSecureText;
                    });
                  },
                  icon: Icon(
                    (isSecureText) ? Icons.visibility_off : Icons.visibility,
                    color: Colors.black87,
                  )),
              hintText: "Masukkan kata sandi"));
    }

    Widget buttonAction(size) {
      return Padding(
          padding: EdgeInsets.symmetric(horizontal: defaultMargin),
          child: ButtonPrimary(
              label: 'MASUK',
              isLoadingBtn: isLoadingBtn,
              onPressed: () {
                validation();
              }));
    }

    Widget dontHaveAccount() {
      return Padding(
          padding: EdgeInsets.symmetric(horizontal: defaultMargin),
          child: Column(children: [
            InkWell(
                borderRadius: BorderRadius.circular(5),
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (_) => const ForgotPasswordPage()));
                },
                child: Text("Lupa Kata Sandi ?",
                    style: mediumFontStyle, textAlign: TextAlign.end)),
            const SizedBox(height: 8),
            Row(mainAxisAlignment: MainAxisAlignment.center, children: [
              Text("Belum memiliki akun? ", style: regulerFontStyle),
              InkWell(
                  borderRadius: BorderRadius.circular(5),
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (_) => const SignUpPage()));
                  },
                  child: Text("Daftar Sekarang", style: boldFontStyle))
            ])
          ]));
    }

    return Scaffold(
        body: ListView(children: [
      const SizedBox(height: 50),
      headerWidget(),
      const SizedBox(height: 30),
      loginText(),
      const SizedBox(height: 15),
      usernameField(),
      const SizedBox(height: 15),
      passwordField(),
      const SizedBox(height: 30),
      buttonAction(size),
      const SizedBox(height: 15),
      dontHaveAccount(),
      const SizedBox(height: 15)
    ]));
  }
}
