part of '../../helpers/shared.dart';

class SignUpPage extends StatefulWidget {
  const SignUpPage({Key? key}) : super(key: key);

  @override
  State<SignUpPage> createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  final TextEditingController nipController = TextEditingController();
  final TextEditingController nameController = TextEditingController();
  final TextEditingController emailController = TextEditingController();
  final TextEditingController phoneController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final TextEditingController passwordConfirmController =
      TextEditingController();
  bool isSecureText = true, isSecureConfirmText = true, isLoadingBtn = false;
  List schools = [];
  String valSchool = "";

  @override
  void dispose() {
    nipController.dispose();
    nameController.dispose();
    emailController.dispose();
    phoneController.dispose();
    passwordController.dispose();
    passwordConfirmController.dispose();
    super.dispose();
  }

  void validation() {
    if (nameController.text.isEmpty ||
        emailController.text.isEmpty ||
        phoneController.text.isEmpty ||
        passwordController.text.isEmpty ||
        passwordConfirmController.text.isEmpty) {
      Helpers.showDialogAlert(
          alertWarning, "Mohon lengkapi form pendaftaran ...", context);
    } else if (!EmailValidator.validate(emailController.text)) {
      Helpers.showDialogAlert(
          alertWarning, "Maaf, Alamat email tidak valid ...", context);
    } else if (phoneController.text.length < 10) {
      Helpers.showDialogAlert(
          alertWarning, "Nomor handphone min 10 angka ...", context);
    } else if (valSchool.isEmpty || valSchool == '') {
      Helpers.showDialogAlert(alertWarning,
          "Mohon pilih nama Sekolah dimana Anda bertugas ...", context);
    } else if (passwordController.text.length < 8 ||
        passwordConfirmController.text.length < 8) {
      Helpers.showDialogAlert(alertWarning,
          "Password dan konfirmasi password min 8 karakter ...", context);
    } else if (passwordController.text != passwordConfirmController.text) {
      Helpers.showDialogAlert(alertWarning,
          "Password tidak sama dengan konfirmasi password ...", context);
    } else {
      register();
    }
  }

  Future<void> getSchool() async {
    List data = [
      {"label": "--Pilih Sekolah--", "value": ""}
    ];
    GlobalModel response =
        await GlobalService.getData(BaseURL.schools, context);
    if (!mounted) return;
    if (response.status == 'success') {
      for (var i = 0; i < response.result.length; i++) {
        data.add({
          "label": response.result[i]['name'],
          "value": response.result[i]['id']
        });
      }
      schools = data;
      setState(() {});
    } else {
      Helpers.showDialogAlert(alertWarning, response.errorMessage, context);
    }
  }

  Future<void> register() async {
    setState(() {
      isLoadingBtn = true;
    });
    Map<String, dynamic> body = {
      "nip": nipController.text,
      "name": nameController.text,
      "phone": phoneController.text,
      "email": emailController.text,
      "password": passwordController.text,
      "school": valSchool
    };
    GlobalModel response =
        await GlobalService.postData(BaseURL.signup, body, context);
    if (!mounted) return;
    if (response.status == 'success') {
      Helpers.showDialogAlertFunction(alertSuccess, response.result, () {
        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (_) => const SignInPage()),
            (route) => false);
      }, context, textButton: 'Login Sekarang');
    } else {
      Helpers.showDialogAlert(alertWarning, response.errorMessage, context);
    }
    setState(() {
      isLoadingBtn = false;
    });
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  void initState() {
    super.initState();
    getSchool();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    Widget headerWidget() {
      return Column(children: [
        Image.asset('assets/images/logo.png', height: 100, width: 100),
        const SizedBox(height: 10),
        Text("Inventaris Sekolah", style: boldFontStyle.copyWith(fontSize: 18))
      ]);
    }

    Widget loginText() {
      return Padding(
          padding: EdgeInsets.symmetric(horizontal: defaultMargin),
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Text("Daftar Akun", style: mediumFontStyle.copyWith(fontSize: 18)),
            const SizedBox(height: 5),
            Text(
                "Daftarkan akun anda sekarang agar dapat mengatur inventaris Peralatan dan Perlengkapan Sekolah",
                style: regulerFontStyle.copyWith(height: 1.5))
          ]));
    }

    Widget nipField() {
      return Padding(
          padding: EdgeInsets.symmetric(horizontal: defaultMargin),
          child: TextFieldBox(
              controller: nipController,
              title: "NIP",
              hintText: "Masukkan nip",
              keyboardType: TextInputType.number,
              inputFormatter: [FilteringTextInputFormatter.digitsOnly]));
    }

    Widget nameField() {
      return Padding(
          padding: EdgeInsets.symmetric(horizontal: defaultMargin),
          child: TextFieldBox(
              controller: nameController,
              title: "Nama Lengkap",
              textCapitalization: TextCapitalization.words,
              hintText: "Masukkan nama lengkap"));
    }

    Widget placeSchool() => Padding(
        padding: EdgeInsets.symmetric(horizontal: defaultMargin),
        child: SelectBox(
            label: 'Tempat Bertugas',
            valueItem: valSchool,
            listItems: schools,
            onChange: (val) {
              valSchool = val;
              setState(() {});
            }));

    Widget emailField() {
      return Padding(
          padding: EdgeInsets.symmetric(horizontal: defaultMargin),
          child: TextFieldBox(
              controller: emailController,
              title: "Alamat Email",
              hintText: "Masukkan alamat email",
              keyboardType: TextInputType.emailAddress));
    }

    Widget phoneField() {
      return Padding(
          padding: EdgeInsets.symmetric(horizontal: defaultMargin),
          child: TextFieldBox(
              controller: phoneController,
              title: "Nomor Handphone",
              hintText: "Masukkan nomor handphone",
              keyboardType: TextInputType.number,
              inputFormatter: [FilteringTextInputFormatter.digitsOnly]));
    }

    Widget passwordField() {
      return Padding(
        padding: EdgeInsets.symmetric(horizontal: defaultMargin),
        child: TextFieldBox(
          controller: passwordController,
          title: "Kata Sandi",
          secureText: isSecureText,
          suffixIcon: IconButton(
              onPressed: () {
                setState(() {
                  isSecureText = !isSecureText;
                });
              },
              icon: Icon(
                (isSecureText) ? Icons.visibility_off : Icons.visibility,
                color: Colors.black87,
              )),
          hintText: "Masukkan kata sandi",
        ),
      );
    }

    Widget passwordConfirmField() {
      return Padding(
        padding: EdgeInsets.symmetric(horizontal: defaultMargin),
        child: TextFieldBox(
          controller: passwordConfirmController,
          title: "Konfirmasi Kata Sandi",
          secureText: isSecureConfirmText,
          suffixIcon: IconButton(
              onPressed: () {
                setState(() {
                  isSecureConfirmText = !isSecureConfirmText;
                });
              },
              icon: Icon(
                (isSecureConfirmText) ? Icons.visibility_off : Icons.visibility,
                color: Colors.black87,
              )),
          hintText: "Masukkan konfirmasi kata sandi",
        ),
      );
    }

    Widget buttonAction(size) {
      return Padding(
          padding: EdgeInsets.symmetric(horizontal: defaultMargin),
          child: ButtonPrimary(
              label: 'DAFTAR',
              isLoadingBtn: isLoadingBtn,
              onPressed: () {
                validation();
              }));
    }

    Widget anyHaveAccount() {
      return Padding(
          padding: EdgeInsets.symmetric(horizontal: defaultMargin),
          child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            Text(
              "Sudah memiliki akun? ",
              style: regulerFontStyle,
            ),
            InkWell(
                borderRadius: BorderRadius.circular(5),
                onTap: () {
                  Navigator.pop(context);
                },
                child: Text("Masuk Sekarang", style: boldFontStyle))
          ]));
    }

    return Scaffold(
        body: ListView(children: [
      const SizedBox(height: 50),
      headerWidget(),
      const SizedBox(height: 30),
      loginText(),
      const SizedBox(height: 15),
      nipField(),
      const SizedBox(height: 15),
      nameField(),
      const SizedBox(height: 15),
      emailField(),
      const SizedBox(height: 15),
      phoneField(),
      const SizedBox(height: 15),
      placeSchool(),
      const SizedBox(height: 15),
      passwordField(),
      const SizedBox(height: 15),
      passwordConfirmField(),
      const SizedBox(height: 30),
      buttonAction(size),
      const SizedBox(height: 15),
      anyHaveAccount(),
      const SizedBox(height: 15)
    ]));
  }
}
