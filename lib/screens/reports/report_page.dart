part of '../../helpers/shared.dart';

class ReportPage extends StatelessWidget {
  const ReportPage({super.key});

  @override
  Widget build(BuildContext context) {
    final scrollControllerProductBorrow = ScrollController();
    final scrollControllerUserBorrow = ScrollController();
    Future<GlobalModel> getReport() async {
      GlobalModel response =
          await GlobalService.getData(BaseURL.reports, context);
      return response;
    }

    Widget dashboardProductWidget(
            String title, String reportModel, Color color) =>
        Expanded(
            child: Container(
                padding:
                    const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10), color: color),
                child: Column(children: [
                  Text(title,
                      textAlign: TextAlign.center,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: regulerFontStyle.copyWith(
                          color: whiteColor, fontWeight: FontWeight.w700)),
                  const SizedBox(height: 5),
                  Text(reportModel,
                      textScaleFactor: 2.0,
                      style: regulerFontStyle.copyWith(
                          color: whiteColor, fontWeight: FontWeight.w700))
                ])));

    Widget productReportBorrow(ReportModel reportModel) =>
        Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Container(
              margin: const EdgeInsets.only(bottom: 10),
              child: Row(children: [
                Expanded(
                    child: Text('Barang Inventaris Yang Banyak Dipinjam',
                        style: regulerFontStyle.copyWith(
                            fontSize: 16, fontWeight: FontWeight.w700))),
                if (reportModel.reportDataProduct!.length > 5)
                  const SizedBox(width: 10),
                if (reportModel.reportDataProduct!.length > 5)
                  InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (_) =>
                                    const ListReportProductBorrowPage()));
                      },
                      child: Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 10, vertical: 5),
                          child: Text('Lihat Semua',
                              style: lightFontStyle.copyWith(
                                  color: primaryColor))))
              ])),
          Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  border: Border.all(width: 0.5, color: greyColor!)),
              child: (reportModel.reportDataProduct!.isEmpty)
                  ? Padding(
                      padding: EdgeInsets.all(defaultMargin),
                      child: Center(
                          child: Text('-- No Data Found --',
                              style: regulerFontStyle)))
                  : Scrollbar(
                      thickness: 2,
                      trackVisibility: true,
                      controller: scrollControllerProductBorrow,
                      thumbVisibility: true,
                      child: SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          controller: scrollControllerProductBorrow,
                          child: DataTable(
                              border: TableBorder.symmetric(
                                  inside: BorderSide(
                                      width: 0.5, color: greyColor!)),
                              columns: const [
                                DataColumn(
                                    label: Expanded(
                                        child: Text('No',
                                            textAlign: TextAlign.center))),
                                DataColumn(
                                    label: Expanded(
                                        child: Text('Kode\nBarang',
                                            textAlign: TextAlign.center))),
                                DataColumn(
                                    label: Expanded(
                                        child: Text('Nama\nBarang',
                                            textAlign: TextAlign.center))),
                                DataColumn(
                                    label: Expanded(
                                        child: Text(
                                            'Jumlah Yang\nDipinjam (Pcs)',
                                            textAlign: TextAlign.center))),
                                DataColumn(
                                    label: Expanded(
                                        child: Text('Action',
                                            textAlign: TextAlign.center)))
                              ],
                              rows: [
                                for (ReportDataProduct product
                                    in reportModel.reportDataProduct!)
                                  DataRow(cells: <DataCell>[
                                    DataCell(Text(
                                        '${reportModel.reportDataProduct!.indexOf(product) + 1}',
                                        textAlign: TextAlign.center)),
                                    DataCell(Text(product.codeProduct ?? '',
                                        textAlign: TextAlign.center)),
                                    DataCell(Text(product.name ?? '',
                                        textAlign: TextAlign.center)),
                                    DataCell(Center(
                                        child: Text(product.total ?? '',
                                            textAlign: TextAlign.center))),
                                    DataCell(Center(
                                        child: ActionChip(
                                            label: Text('Detail',
                                                style: regulerFontStyle),
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(5)),
                                            onPressed: () {
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (_) =>
                                                          DetailInventoryPage(
                                                              id:
                                                                  '${product.productId}',
                                                              isFromReport:
                                                                  true)));
                                            })))
                                  ])
                              ])))),
          SizedBox(height: defaultMargin)
        ]);

    Widget usersReportBorrow(ReportModel reportModel) =>
        Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Container(
              margin: const EdgeInsets.only(bottom: 10),
              child: Row(children: [
                Expanded(
                    child: Text('Data Peminjam Barang Terbanyak',
                        style: regulerFontStyle.copyWith(
                            fontSize: 16, fontWeight: FontWeight.w700))),
                if (reportModel.reportUsersBorrow!.length > 5)
                  const SizedBox(width: 10),
                if (reportModel.reportUsersBorrow!.length > 5)
                  InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (_) =>
                                    const ListReportUsersBorrowPage()));
                      },
                      child: Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 10, vertical: 5),
                          child: Text('Lihat Semua',
                              style: lightFontStyle.copyWith(
                                  color: primaryColor))))
              ])),
          Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  border: Border.all(width: 0.5, color: greyColor!)),
              child: (reportModel.reportDataProduct!.isEmpty)
                  ? Padding(
                      padding: EdgeInsets.all(defaultMargin),
                      child: Center(
                          child: Text('-- No Data Found --',
                              style: regulerFontStyle)))
                  : Scrollbar(
                      thickness: 2,
                      trackVisibility: true,
                      controller: scrollControllerUserBorrow,
                      thumbVisibility: true,
                      child: SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          controller: scrollControllerUserBorrow,
                          child: DataTable(
                              border: TableBorder.symmetric(
                                  inside: BorderSide(
                                      width: 0.5, color: greyColor!)),
                              columns: const [
                                DataColumn(
                                    label: Expanded(
                                        child: Text('No',
                                            textAlign: TextAlign.center))),
                                DataColumn(
                                    label: Expanded(
                                        child: Text('Nama\nPeminjam',
                                            textAlign: TextAlign.center))),
                                DataColumn(
                                    label: Expanded(
                                        child: Text('Nama\nSekolah',
                                            textAlign: TextAlign.center))),
                                DataColumn(
                                    label: Expanded(
                                        child: Text(
                                            'Jumlah Yang\nDipinjam (Pcs)',
                                            textAlign: TextAlign.center))),
                                DataColumn(
                                    label: Expanded(
                                        child: Text('Action',
                                            textAlign: TextAlign.center)))
                              ],
                              rows: [
                                for (ReportUsersBorrow user
                                    in reportModel.reportUsersBorrow!)
                                  DataRow(cells: <DataCell>[
                                    DataCell(Text(
                                        '${reportModel.reportUsersBorrow!.indexOf(user) + 1}',
                                        textAlign: TextAlign.center)),
                                    DataCell(Text(user.name ?? '',
                                        textAlign: TextAlign.center)),
                                    DataCell(Text(user.schoolName ?? '',
                                        textAlign: TextAlign.center)),
                                    DataCell(Center(
                                        child: Text(user.total ?? '',
                                            textAlign: TextAlign.center))),
                                    DataCell(Center(
                                        child: ActionChip(
                                            label: Text('Detail',
                                                style: regulerFontStyle),
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(5)),
                                            onPressed: () {
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (_) =>
                                                          DetailReportUserBorrowPage(
                                                              userID:
                                                                  user.userId ??
                                                                      '')));
                                            })))
                                  ])
                              ]))))
        ]);

    return Scaffold(
        appBar: AppBar(title: const Text('Laporan Inventaris')),
        body: FutureBuilder<GlobalModel>(
            future: getReport(),
            builder: (context, snapshot) {
              GlobalModel? data = snapshot.data;
              if (snapshot.connectionState == ConnectionState.done) {
                if (data?.status == 'success') {
                  ReportModel reportModel = ReportModel.fromJson(data?.result);

                  return ListView(
                      padding: EdgeInsets.all(defaultMargin),
                      children: [
                        Container(
                            margin: EdgeInsets.only(bottom: defaultMargin),
                            child: Row(children: [
                              dashboardProductWidget(
                                  'Inventaris Tersedia',
                                  '${reportModel.productAll}',
                                  const Color(0XFF0A4F9D)),
                              const SizedBox(width: 5),
                              dashboardProductWidget(
                                  'Inventaris\nDipinjamkan',
                                  '${reportModel.productBorrow}',
                                  const Color(0XFFF8863F)),
                              const SizedBox(width: 5),
                              dashboardProductWidget(
                                  'Inventaris\nSaat Ini',
                                  '${reportModel.productExisting}',
                                  const Color(0XFF22C55E))
                            ])),
                        productReportBorrow(reportModel),
                        usersReportBorrow(reportModel)
                      ]);
                }
                return Center(child: Text(data?.errorMessage ?? ''));
              }
              return const Center(child: CircularProgressIndicator());
            }));
  }
}
