import 'package:flutter/material.dart';
import 'package:inventaris_school_app/helpers/models.dart';
import 'package:inventaris_school_app/helpers/shared.dart';
import 'package:inventaris_school_app/helpers/theme.dart';

class DetailReportUserBorrowPage extends StatelessWidget {
  final String userID;
  const DetailReportUserBorrowPage({super.key, required this.userID});

  @override
  Widget build(BuildContext context) {
    Future<GlobalModel> getDetail() async {
      GlobalModel response = await GlobalService.getData(
          '${BaseURL.reportDetailUserBorrow}$userID', context);
      return response;
    }

    Widget itemRow(String label, String sublabel) => Container(
        margin: const EdgeInsets.only(bottom: 10),
        child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(child: Text(label, style: regulerFontStyle)),
              Text(": ", style: regulerFontStyle),
              Expanded(
                  child: Text(sublabel,
                      textAlign: TextAlign.start, style: mediumFontStyle))
            ]));

    return Scaffold(
        appBar: AppBar(title: const Text('Laporan Peminjam')),
        body: FutureBuilder(
            future: getDetail(),
            builder: (context, snapshot) {
              GlobalModel? data = snapshot.data;

              if (snapshot.connectionState == ConnectionState.done) {
                if (data?.status == 'success') {
                  DetailReportUserBorrow detailReportUserBorrow =
                      DetailReportUserBorrow.fromJson(data?.result);
                  return ListView(
                      padding: EdgeInsets.all(defaultMargin),
                      children: [
                        itemRow('NIP', detailReportUserBorrow.nip ?? ''),
                        itemRow(
                            'Nama Lengkap', detailReportUserBorrow.name ?? ''),
                        itemRow('Nomor Telepon/HP',
                            detailReportUserBorrow.phone ?? ''),
                        itemRow(
                            'Alamat Email', detailReportUserBorrow.email ?? ''),
                        itemRow(
                            'Jabatan', detailReportUserBorrow.roleName ?? ''),
                        itemRow('Tempat Bertugas',
                            detailReportUserBorrow.schoolName ?? ''),
                        Container(
                            margin: const EdgeInsets.only(bottom: 10),
                            child: Text('Barang Inventaris Yang Dipinjam :',
                                style: regulerFontStyle)),
                        for (DataProduct product
                            in detailReportUserBorrow.dataProduct!)
                          ListTile(
                              contentPadding: EdgeInsets.zero,
                              leading: Container(
                                  height: 70,
                                  width: 50,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                      boxShadow: [
                                        BoxShadow(
                                            color: greyColor ?? Colors.white,
                                            offset: const Offset(0.0, 0.0),
                                            blurRadius: 0.5,
                                            spreadRadius: 0.0) //BoxShadow
                                      ],
                                      image: DecorationImage(
                                          fit: BoxFit.cover,
                                          image: NetworkImage(
                                              '${BaseURL.apiAssets}/${product.image}')))),
                              title: Text(
                                  // '${product.codeProduct ?? ''} - ${product.productName ?? ''}'),
                                  product.productName ?? '',
                                  overflow: TextOverflow.ellipsis,
                                  style: regulerFontStyle),
                              subtitle: Text(
                                  '${product.categoryName ?? ''} - ${product.quantity ?? ''} Barang',
                                  overflow: TextOverflow.ellipsis,
                                  style: regulerFontStyle),
                              trailing: Text('${product.status}',
                                  overflow: TextOverflow.ellipsis,
                                  style:
                                      regulerFontStyle.copyWith(fontSize: 14)))
                      ]);
                }
                return Center(child: Text(data?.errorMessage ?? ''));
              }
              return const Center(child: CircularProgressIndicator());
            }));
  }
}
