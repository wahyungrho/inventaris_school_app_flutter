import 'package:flutter/material.dart';
import 'package:inventaris_school_app/helpers/models.dart';
import 'package:inventaris_school_app/helpers/shared.dart';
import 'package:inventaris_school_app/helpers/theme.dart';

class ListReportUsersBorrowPage extends StatelessWidget {
  const ListReportUsersBorrowPage({super.key});

  @override
  Widget build(BuildContext context) {
    final scrollControllerUsersBorrow = ScrollController();

    Future<GlobalModel> getList() async {
      GlobalModel response =
          await GlobalService.getData(BaseURL.reportListUserBorrow, context);
      return response;
    }

    Widget usersReportBorrow(List<ListReportUserBorrow> data) =>
        Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Container(
              margin: const EdgeInsets.only(bottom: 10),
              child: Text('Data Peminjam Barang Terbanyak',
                  style: regulerFontStyle.copyWith(
                      fontSize: 16, fontWeight: FontWeight.w700))),
          Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  border: Border.all(width: 0.5, color: greyColor!)),
              child: (data.isEmpty)
                  ? Padding(
                      padding: EdgeInsets.all(defaultMargin),
                      child: Center(
                          child: Text('-- No Data Found --',
                              style: regulerFontStyle)))
                  : Scrollbar(
                      thickness: 2,
                      trackVisibility: true,
                      controller: scrollControllerUsersBorrow,
                      thumbVisibility: true,
                      child: SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          controller: scrollControllerUsersBorrow,
                          child: DataTable(
                              border: TableBorder.symmetric(
                                  inside: BorderSide(
                                      width: 0.5, color: greyColor!)),
                              columns: const [
                                DataColumn(
                                    label: Expanded(
                                        child: Text('No',
                                            textAlign: TextAlign.center))),
                                DataColumn(
                                    label: Expanded(
                                        child: Text('Nama\nPeminjam',
                                            textAlign: TextAlign.center))),
                                DataColumn(
                                    label: Expanded(
                                        child: Text('Nama\nSekolah',
                                            textAlign: TextAlign.center))),
                                DataColumn(
                                    label: Expanded(
                                        child: Text(
                                            'Jumlah Yang\nDipinjam (Pcs)',
                                            textAlign: TextAlign.center))),
                                DataColumn(
                                    label: Expanded(
                                        child: Text('Action',
                                            textAlign: TextAlign.center)))
                              ],
                              rows: [
                                for (ListReportUserBorrow user in data)
                                  DataRow(cells: <DataCell>[
                                    DataCell(Text('${data.indexOf(user) + 1}',
                                        textAlign: TextAlign.center)),
                                    DataCell(Text(user.name ?? '',
                                        textAlign: TextAlign.center)),
                                    DataCell(Text(user.schoolName ?? '',
                                        textAlign: TextAlign.center)),
                                    DataCell(Center(
                                        child: Text(user.total ?? '',
                                            textAlign: TextAlign.center))),
                                    DataCell(Center(
                                        child: ActionChip(
                                            label: Text('Detail',
                                                style: regulerFontStyle),
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(5)),
                                            onPressed: () {})))
                                  ])
                              ]))))
        ]);

    return Scaffold(
        appBar: AppBar(title: const Text('Laporan Peminjam')),
        body: FutureBuilder(
            future: getList(),
            builder: (context, snapshot) {
              GlobalModel? data = snapshot.data;
              List<ListReportUserBorrow> reportUsers = [];

              if (snapshot.connectionState == ConnectionState.done) {
                if (data?.status == 'success') {
                  for (dynamic item in data?.result) {
                    reportUsers.add(ListReportUserBorrow.fromJson(item));
                  }

                  return ListView(
                      padding: EdgeInsets.all(defaultMargin),
                      children: [usersReportBorrow(reportUsers)]);
                }
                return Center(child: Text(data?.errorMessage ?? ''));
              }
              return const Center(child: CircularProgressIndicator());
            }));
  }
}
