import 'package:flutter/material.dart';
import 'package:inventaris_school_app/helpers/models.dart';
import 'package:inventaris_school_app/helpers/shared.dart';
import 'package:inventaris_school_app/helpers/theme.dart';

class ListReportProductBorrowPage extends StatelessWidget {
  const ListReportProductBorrowPage({super.key});

  @override
  Widget build(BuildContext context) {
    final scrollControllerProductBorrow = ScrollController();

    Future<GlobalModel> getList() async {
      GlobalModel response =
          await GlobalService.getData(BaseURL.reportListProduct, context);
      return response;
    }

    Widget productReportBorrow(List<ListReportProductBorrow> data) =>
        Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Container(
              margin: const EdgeInsets.only(bottom: 10),
              child: Text('Barang Inventaris Yang Banyak Dipinjam',
                  style: regulerFontStyle.copyWith(
                      fontSize: 16, fontWeight: FontWeight.w700))),
          Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  border: Border.all(width: 0.5, color: greyColor!)),
              child: (data.isEmpty)
                  ? Padding(
                      padding: EdgeInsets.all(defaultMargin),
                      child: Center(
                          child: Text('-- No Data Found --',
                              style: regulerFontStyle)))
                  : Scrollbar(
                      thickness: 2,
                      trackVisibility: true,
                      controller: scrollControllerProductBorrow,
                      thumbVisibility: true,
                      child: SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          controller: scrollControllerProductBorrow,
                          child: DataTable(
                              border: TableBorder.symmetric(
                                  inside: BorderSide(
                                      width: 0.5, color: greyColor!)),
                              columns: const [
                                DataColumn(
                                    label: Expanded(
                                        child: Text('No',
                                            textAlign: TextAlign.center))),
                                DataColumn(
                                    label: Expanded(
                                        child: Text('Kode\nBarang',
                                            textAlign: TextAlign.center))),
                                DataColumn(
                                    label: Expanded(
                                        child: Text('Nama\nBarang',
                                            textAlign: TextAlign.center))),
                                DataColumn(
                                    label: Expanded(
                                        child: Text(
                                            'Jumlah Yang\nDipinjam (Pcs)',
                                            textAlign: TextAlign.center))),
                                DataColumn(
                                    label: Expanded(
                                        child: Text('Action',
                                            textAlign: TextAlign.center)))
                              ],
                              rows: [
                                for (ListReportProductBorrow product in data)
                                  DataRow(cells: <DataCell>[
                                    DataCell(Text(
                                        '${data.indexOf(product) + 1}',
                                        textAlign: TextAlign.center)),
                                    DataCell(Text(product.codeProduct ?? '',
                                        textAlign: TextAlign.center)),
                                    DataCell(Text(product.name ?? '',
                                        textAlign: TextAlign.center)),
                                    DataCell(Center(
                                        child: Text(product.total ?? '',
                                            textAlign: TextAlign.center))),
                                    DataCell(Center(
                                        child: ActionChip(
                                            label: Text('Detail',
                                                style: regulerFontStyle),
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(5)),
                                            onPressed: () {
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (_) =>
                                                          DetailInventoryPage(
                                                              id:
                                                                  '${product.productId}',
                                                              isFromReport:
                                                                  true)));
                                            })))
                                  ])
                              ])))),
          SizedBox(height: defaultMargin)
        ]);

    return Scaffold(
        appBar: AppBar(title: const Text('Laporan Barang Inventaris')),
        body: FutureBuilder(
            future: getList(),
            builder: (context, snapshot) {
              GlobalModel? data = snapshot.data;
              List<ListReportProductBorrow> reportProducts = [];

              if (snapshot.connectionState == ConnectionState.done) {
                if (data?.status == 'success') {
                  for (dynamic item in data?.result) {
                    reportProducts.add(ListReportProductBorrow.fromJson(item));
                  }

                  return ListView(
                      padding: EdgeInsets.all(defaultMargin),
                      children: [productReportBorrow(reportProducts)]);
                }
                return Center(child: Text(data?.errorMessage ?? ''));
              }
              return const Center(child: CircularProgressIndicator());
            }));
  }
}
