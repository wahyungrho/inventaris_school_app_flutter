part of '../../helpers/shared.dart';

class DetailHistoryPage extends StatefulWidget {
  final HistoryLoanModel historyLoanModel;
  final VoidCallback? callback;
  const DetailHistoryPage(
      {super.key, required this.historyLoanModel, this.callback});

  @override
  State<DetailHistoryPage> createState() => _DetailHistoryPageState();
}

class _DetailHistoryPageState extends State<DetailHistoryPage> {
  bool isLoading = false;

  Future<void> updateStatus(String type) async {
    setState(() {
      isLoading = true;
    });
    GlobalModel response = await GlobalService.postData(
        BaseURL.updateLoanInventaris,
        {"order_id": widget.historyLoanModel.orderID, 'type': type},
        context);
    if (!mounted) return;
    if (response.status == 'success') {
      Helpers.showDialogAlertFunction(alertSuccess, response.result, () {
        widget.callback!();
        Navigator.pop(context);
        Navigator.pop(context);
      }, context);
    } else {
      Helpers.showDialogAlert(alertWarning, response.errorMessage, context);
    }
    setState(() {
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text('Detail Riwayat Peminjaman')),
        body: ListView(padding: EdgeInsets.all(defaultMargin), children: [
          itemRow(
              'Status Peminjaman', widget.historyLoanModel.statusName ?? ''),
          itemRow(
              'Tanggal Peminjaman', widget.historyLoanModel.createLoan ?? ''),
          Divider(color: greyColor, height: 0),
          const SizedBox(height: 10),
          Text("Data Peminjam", style: boldFontStyle),
          const SizedBox(height: 10),
          itemRow('Nama Peminjam', widget.historyLoanModel.name ?? ''),
          itemRow('Nomor Telepon', widget.historyLoanModel.phone ?? ''),
          itemRow('Alamat Email', widget.historyLoanModel.email ?? ''),
          itemRow('Jabatan', widget.historyLoanModel.roleName ?? ''),
          itemRow('Tempat Bertugas', widget.historyLoanModel.schoolName ?? ''),
          const SizedBox(height: 10),
          Divider(color: greyColor, height: 0),
          const SizedBox(height: 10),
          Text("Barang Dipinjam", style: boldFontStyle),
          const SizedBox(height: 10),
          ListTile(
              leading: Container(
                  height: 70,
                  width: 50,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: [
                        BoxShadow(
                          color: greyColor ?? Colors.white,
                          offset: const Offset(0.0, 0.0),
                          blurRadius: 0.5,
                          spreadRadius: 0.0,
                        ), //BoxShadow
                      ],
                      image: DecorationImage(
                          fit: BoxFit.cover,
                          image: NetworkImage(
                              '${BaseURL.apiAssets}/${widget.historyLoanModel.image}')))),
              title: Text(widget.historyLoanModel.productName ?? ''),
              subtitle: Text(widget.historyLoanModel.categoryName ?? '',
                  overflow: TextOverflow.ellipsis),
              trailing: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text('Total Pinjaman :',
                        style: lightFontStyle, overflow: TextOverflow.ellipsis),
                    Text('${widget.historyLoanModel.quantity ?? ''} Barang',
                        overflow: TextOverflow.ellipsis)
                  ])),
          if (widget.historyLoanModel.notes != '') const SizedBox(height: 10),
          if (widget.historyLoanModel.notes != '')
            Divider(color: greyColor, height: 0),
          if (widget.historyLoanModel.notes != '') const SizedBox(height: 10),
          if (widget.historyLoanModel.notes != '')
            Text("Keterangan Peminjaman", style: boldFontStyle),
          if (widget.historyLoanModel.notes != '') const SizedBox(height: 10),
          if (widget.historyLoanModel.notes != '')
            Text(widget.historyLoanModel.notes ?? '', style: regulerFontStyle),
          SizedBox(height: defaultMargin),
          if (widget.historyLoanModel.roleName != 'GURU' &&
              widget.historyLoanModel.statusName == 'Menunggu Persetujuan')
            Container(
                margin: const EdgeInsets.only(bottom: 10),
                child: ButtonPrimary(
                    isLoadingBtn: isLoading,
                    label: 'PEMINJAMAN DISETUJUI',
                    onPressed: () {
                      Helpers.showDialogConfirmationFunction(alertWarning,
                          'Apakah kamu yakin menyetujui pengajuan ini ?', () {
                        updateStatus('disetujui');
                      }, context);
                    })),
          if (widget.historyLoanModel.roleName != 'GURU' &&
              widget.historyLoanModel.statusName == 'Peminjaman Disetujui')
            Container(
                margin: const EdgeInsets.only(bottom: 10),
                child: ButtonPrimary(
                    isLoadingBtn: isLoading,
                    label: 'PENGEMBALIAN BARANG',
                    onPressed: () {
                      Helpers.showDialogConfirmationFunction(alertWarning,
                          'Apakah kamu yakin barang ini dikembalikan?', () {
                        updateStatus('dikembalikan');
                      }, context);
                    })),
          if (widget.historyLoanModel.roleName == 'GURU' &&
              widget.historyLoanModel.statusName == 'Menunggu Persetujuan')
            Container(
                margin: const EdgeInsets.only(bottom: 10),
                child: ButtonOutline(
                    isLoadingBtn: isLoading,
                    label: 'BATALKAN PEMINJAMAN',
                    onPressed: () {
                      Helpers.showDialogConfirmationFunction(alertWarning,
                          'Apakah kamu yakin pengajuan ini dibatalkan ?', () {
                        updateStatus('dibatalkan');
                      }, context);
                    })),
          if (widget.historyLoanModel.roleName != 'GURU' &&
              widget.historyLoanModel.statusName == 'Menunggu Persetujuan')
            Container(
                margin: const EdgeInsets.only(bottom: 10),
                child: ButtonOutline(
                    isLoadingBtn: isLoading,
                    label: 'PEMINJAMAN TIDAK DISETUJUI',
                    onPressed: () {
                      Helpers.showDialogConfirmationFunction(alertWarning,
                          'Apakah kamu yakin pengajuan ini tidak disetujui ?',
                          () {
                        updateStatus('tidak_disetujui');
                      }, context);
                    })),
        ]));
  }

  Widget itemRow(String label, String sublabel) => Container(
        margin: const EdgeInsets.only(bottom: 10),
        child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(child: Text(label, style: regulerFontStyle)),
              Text(": ", style: regulerFontStyle),
              Expanded(
                  child: Text(sublabel,
                      textAlign: TextAlign.start, style: mediumFontStyle))
            ]),
      );
}
