part of '../../helpers/shared.dart';

class HistoryLoanPage extends StatefulWidget {
  final UserModel? userModel;
  const HistoryLoanPage({super.key, this.userModel});

  @override
  State<HistoryLoanPage> createState() => _HistoryLoanPageState();
}

class _HistoryLoanPageState extends State<HistoryLoanPage> {
  List status = [];
  String valStatus = "";

  Future<void> getStatus() async {
    List data = [
      {"label": "--Pilih Status--", "value": ""}
    ];
    GlobalModel response = await GlobalService.getData(BaseURL.status, context);
    if (!mounted) return;
    if (response.status == 'success') {
      for (var i = 0; i < response.result.length; i++) {
        data.add({
          "label": response.result[i]['status'],
          "value": response.result[i]['status']
        });
      }
      status = data;
      setState(() {});
    } else {
      Helpers.showDialogAlert(alertWarning, response.errorMessage, context);
    }
  }

  Future<GlobalModel> getLoanList() async {
    GlobalModel response = await GlobalService.getData(
        '${BaseURL.listLoanInventaris}?user_id=${(widget.userModel?.roleName == 'GURU') ? widget.userModel?.id ?? '' : ''}&status=$valStatus',
        context);
    return response;
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  void initState() {
    super.initState();
    getStatus();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text('Riwayat Peminjaman')),
        body: Column(
          children: [
            Padding(
                padding: EdgeInsets.all(defaultMargin),
                child: SelectBox(
                    label: 'Status Peminjaman',
                    valueItem: valStatus,
                    listItems: status,
                    onChange: (val) {
                      valStatus = val;
                      setState(() {});
                    })),
            Divider(color: greyColor, height: 0),
            Expanded(
                child: FutureBuilder<GlobalModel>(
                    future: getLoanList(),
                    builder: (context, snapshot) {
                      List<HistoryLoanModel> list = [];
                      if (snapshot.connectionState == ConnectionState.done) {
                        if (snapshot.data?.status == 'success') {
                          for (var item in snapshot.data?.result) {
                            list.add(HistoryLoanModel.fromJson(item));
                          }
                          if (list.isNotEmpty) {
                            return ListView(
                                padding: const EdgeInsets.all(16),
                                children: list
                                    .map((e) => InkWell(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          onTap: () {
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (_) =>
                                                        DetailHistoryPage(
                                                            callback: () {
                                                              setState(() {});
                                                            },
                                                            historyLoanModel:
                                                                e)));
                                          },
                                          child: Card(
                                              child: Padding(
                                                  padding:
                                                      const EdgeInsets.all(8.0),
                                                  child: Column(
                                                    children: [
                                                      Row(children: [
                                                        Expanded(
                                                            child: Text(
                                                                e.createLoan ??
                                                                    '',
                                                                style:
                                                                    lightFontStyle)),
                                                        Expanded(
                                                            child: Text(
                                                                e.statusName ??
                                                                    '',
                                                                textAlign:
                                                                    TextAlign
                                                                        .end,
                                                                style:
                                                                    mediumFontStyle)),
                                                      ]),
                                                      const SizedBox(height: 5),
                                                      Divider(
                                                          color: greyColor,
                                                          height: 0),
                                                      const SizedBox(height: 5),
                                                      ListTile(
                                                        leading: Container(
                                                            height: 70,
                                                            width: 50,
                                                            decoration:
                                                                BoxDecoration(
                                                                    borderRadius:
                                                                        BorderRadius.circular(
                                                                            10),
                                                                    boxShadow: [
                                                                      BoxShadow(
                                                                          color: greyColor ??
                                                                              Colors
                                                                                  .white,
                                                                          offset: const Offset(0.0,
                                                                              0.0),
                                                                          blurRadius:
                                                                              0.5,
                                                                          spreadRadius:
                                                                              0.0) //BoxShadow
                                                                    ],
                                                                    image: DecorationImage(
                                                                        fit: BoxFit
                                                                            .cover,
                                                                        image: NetworkImage(
                                                                            '${BaseURL.apiAssets}/${e.image}')))),
                                                        title: Text(
                                                            e.productName ?? '',
                                                            overflow:
                                                                TextOverflow
                                                                    .ellipsis),
                                                        subtitle: Text(
                                                            e.categoryName ??
                                                                '',
                                                            overflow:
                                                                TextOverflow
                                                                    .ellipsis),
                                                        trailing: Column(
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .center,
                                                            children: [
                                                              Text(
                                                                  'Total Pinjaman :',
                                                                  style:
                                                                      lightFontStyle,
                                                                  overflow:
                                                                      TextOverflow
                                                                          .ellipsis),
                                                              Text(
                                                                  '${e.quantity ?? ''} Barang',
                                                                  overflow:
                                                                      TextOverflow
                                                                          .ellipsis)
                                                            ]),
                                                      ),
                                                    ],
                                                  ))),
                                        ))
                                    .toList());
                          }
                          return Padding(
                            padding: EdgeInsets.only(top: defaultMargin),
                            child:
                                const Text('-- Tidak ada data Peminjaman --'),
                          );
                        }
                        return Center(child: Text(snapshot.data?.errorMessage));
                      }
                      return const Center(child: CircularProgressIndicator());
                    }))
          ],
        ));
  }
}
