part of '../helpers/shared.dart';

class MainPage extends StatefulWidget {
  const MainPage({super.key});

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  bool isLoading = false;
  UserModel userModel = UserModel();
  Future<void> userdetail() async {
    SharedPreferences preferences = await GlobalService.sharedPreference;
    String? userID = preferences.getString(PreferenceModel.userID) ?? '';
    setState(() {
      isLoading = true;
    });
    if (!mounted) return;
    GlobalModel response =
        await GlobalService.getData('${BaseURL.userDetail}$userID', context);
    if (response.status == 'success') {
      userModel = UserModel.fromJson(response.result);
      setState(() {});
    } else {
      if (!mounted) return;
      Helpers.showDialogAlert(alertWarning, response.errorMessage, context);
    }
    setState(() {
      isLoading = false;
    });
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  void initState() {
    super.initState();
    userdetail();
  }

  @override
  Widget build(BuildContext context) {
    Widget cardListTileWidget(
            Widget child, String title, String subtitle, Function onTap) =>
        Container(
          margin: const EdgeInsets.only(bottom: 15),
          child: Card(
              margin: EdgeInsets.symmetric(horizontal: defaultMargin),
              child: ListTile(
                  onTap: () {
                    onTap();
                  },
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                  leading: SizedBox(height: double.infinity, child: child),
                  title: Text(title),
                  subtitle: Text(subtitle))),
        );

    Widget headerWidget() => GestureDetector(
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (_) =>
                      ProfilePage(userModel: userModel, callback: userdetail)));
        },
        child: Container(
            padding: EdgeInsets.all(defaultMargin),
            margin: EdgeInsets.only(bottom: defaultMargin),
            width: double.infinity,
            color: primaryColor,
            child: Row(children: [
              const CircleAvatar(child: Icon(Icons.person_outline)),
              const SizedBox(width: 16),
              Expanded(
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                    Text(userModel.name ?? '',
                        style: mediumFontStyle.copyWith(
                            color: whiteColor, fontSize: 16)),
                    Text('${userModel.roleName} - ${userModel.schoolName}',
                        style: lightFontStyle.copyWith(color: whiteColor))
                  ])),
              const SizedBox(width: 16),
              Icon(Icons.chevron_right, color: whiteColor)
            ])));

    return Scaffold(
        body: LoadingOverlay(
            isLoading: isLoading,
            child: ListView(children: [
              headerWidget(),
              cardListTileWidget(
                  const Icon(Icons.history_edu_outlined),
                  'Riwayat Peminjaman',
                  'Riwayat peminjaman barang inventaris', () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (_) => HistoryLoanPage(userModel: userModel)));
              }),
              cardListTileWidget(const Icon(Icons.inventory_2_outlined),
                  'Inventaris', 'Peralatan dan perlengkapan sekolah', () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (_) => ListInventoryPage(
                            roleName: userModel.roleName ?? '')));
              }),
              if (userModel.roleName == 'YAYASAN')
                cardListTileWidget(const Icon(Icons.inventory_outlined),
                    'Kategori Barang', 'Kategori barang inventaris', () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (_) => const ListCategoryPage()));
                }),
              if (userModel.roleName == 'YAYASAN')
                cardListTileWidget(const Icon(Icons.assessment_outlined),
                    'Laporan Inventaris', 'Laporan barang inventaris', () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (_) => const ReportPage()));
                }),
              cardListTileWidget(Icon(Icons.logout, color: redColor), 'Signout',
                  'Keluar dari aplikasi', () {
                Helpers.showDialogConfirmationFunction(
                    alertWarning, 'Apakah kamu yakin untuk keluar ?', () {
                  GlobalService.logout(context);
                }, context);
              })
            ])));
  }
}
