part of '../../helpers/shared.dart';

class ListCategoryPage extends StatefulWidget {
  const ListCategoryPage({super.key});

  @override
  State<ListCategoryPage> createState() => _ListCategoryPageState();
}

class _ListCategoryPageState extends State<ListCategoryPage> {
  final TextEditingController nameController = TextEditingController();

  Future<void> submit(String id, String name) async {
    GlobalModel response = await GlobalService.postData(
        BaseURL.formCategories, {'id': id, 'name': name}, context);
    if (!mounted) return;
    if (response.status == 'success') {
      Helpers.showDialogAlertFunction(alertSuccess, response.result, () {
        Navigator.pop(context);
      }, context);
    } else {
      Helpers.showDialogAlert(alertWarning, response.errorMessage, context);
    }
  }

  Future<void> delete(String id) async {
    GlobalModel response = await GlobalService.postData(
        BaseURL.deleteCategories, {'id': id}, context);
    if (!mounted) return;
    if (response.status == 'success') {
      Helpers.showDialogAlertFunction(alertSuccess, response.result, () {
        Navigator.pop(context);
      }, context);
    } else {
      Helpers.showDialogAlert(alertWarning, response.errorMessage, context);
    }
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  void dispose() {
    nameController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Future<GlobalModel> getCategories() async {
      GlobalModel response =
          await GlobalService.getData(BaseURL.categories, context);
      return response;
    }

    Future<void> showDialogForm(String? id, String? name) async {
      nameController.clear();
      if (id != '') {
        nameController.text = name ?? '';
      }
      return showDialog<void>(
          context: context,
          barrierDismissible: false, // user must tap button!
          builder: (BuildContext context) {
            return AlertDialog(
                title: Text((id == '') ? 'Tambah Kategori' : 'Ubah Kategori'),
                content: SingleChildScrollView(
                    child: ListBody(children: <Widget>[
                  TextFieldBox(
                      controller: nameController,
                      title: 'Nama Kategori',
                      hintText: 'Masukkan nama kategori',
                      textCapitalization: TextCapitalization.words)
                ])),
                actions: <Widget>[
                  Row(children: [
                    if (id != "")
                      Expanded(
                          child: ButtonOutline(
                              label: 'HAPUS',
                              onPressed: () {
                                Helpers.showDialogConfirmationFunction(
                                    alertWarning,
                                    'Yakin untuk menghapus data ini ?',
                                    () async {
                                  Navigator.pop(context);
                                  await delete(id ?? '');
                                  setState(() {});
                                }, context);
                              })),
                    if (id != "") const SizedBox(width: 10),
                    Expanded(
                        child: ButtonPrimary(
                            label: 'SIMPAN',
                            onPressed: () async {
                              if (nameController.text.isEmpty) {
                                Helpers.showDialogAlert(
                                    alertWarning,
                                    "Nama kategori tidak boleh kosong ...",
                                    context);
                              } else {
                                Navigator.of(context).pop();
                                await submit(id ?? '', nameController.text);
                                setState(() {});
                              }
                            }))
                  ]),
                  SizedBox(
                      width: double.infinity,
                      child: TextButton(
                          child: Text('KEMBALI',
                              textAlign: TextAlign.center,
                              style: mediumFontStyle.copyWith(color: redColor)),
                          onPressed: () {
                            Navigator.of(context).pop();
                          }))
                ]);
          });
    }

    return Scaffold(
        appBar: AppBar(title: const Text('Kategori Barang Inventaris')),
        body: FutureBuilder<GlobalModel>(
            future: getCategories(),
            builder: (context, snapshot) {
              List<CategoryModel> list = [];
              if (snapshot.connectionState == ConnectionState.done) {
                if (snapshot.data?.status == 'success') {
                  for (var item in snapshot.data?.result) {
                    list.add(CategoryModel.fromJson(item));
                  }
                  if (list.isNotEmpty) {
                    return ListView(
                        children: list
                            .map((e) => Column(children: [
                                  ListTile(
                                      onTap: () {
                                        showDialogForm(e.id, e.name);
                                      },
                                      title: Text(e.name ?? '',
                                          overflow: TextOverflow.ellipsis),
                                      trailing:
                                          const Icon(Icons.chevron_right)),
                                  Divider(color: greyColor, height: 0),
                                ]))
                            .toList());
                  }
                  return Padding(
                      padding: EdgeInsets.only(top: defaultMargin),
                      child: const Text('-- Tidak ada data Kategori --'));
                }
                return Center(child: Text(snapshot.data?.errorMessage));
              }
              return const Center(child: CircularProgressIndicator());
            }),
        floatingActionButton: FloatingActionButton(
            onPressed: () {
              showDialogForm("", "");
            },
            child: const Icon(Icons.add)));
  }
}
