part of '../helpers/shared.dart';

class ProfilePage extends StatefulWidget {
  final UserModel userModel;
  final VoidCallback callback;
  const ProfilePage(
      {super.key, required this.userModel, required this.callback});

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  final TextEditingController nipController = TextEditingController();
  final TextEditingController nameController = TextEditingController();
  final TextEditingController phoneController = TextEditingController();
  final TextEditingController emailController = TextEditingController();
  final TextEditingController jabatanController = TextEditingController();
  final TextEditingController schoolController = TextEditingController();

  bool isLoadingPage = false;

  Future<void> update() async {
    setState(() {
      isLoadingPage = true;
    });
    GlobalModel response = await GlobalService.postData(
        BaseURL.userUpdate,
        {
          'id': widget.userModel.id,
          'name': nameController.text,
          'email': emailController.text,
          'phone': phoneController.text
        },
        context);
    if (!mounted) return;
    if (response.status == 'success') {
      widget.callback();
      Helpers.showDialogAlert(alertSuccess, response.result, context);
    } else {
      Helpers.showDialogAlert(alertWarning, response.errorMessage, context);
    }
    setState(() {
      isLoadingPage = false;
    });
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  void dispose() {
    nipController.dispose();
    nameController.dispose();
    phoneController.dispose();
    emailController.dispose();
    jabatanController.dispose();
    schoolController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    nipController.text = widget.userModel.nip ?? '';
    nameController.text = widget.userModel.name ?? '';
    phoneController.text = widget.userModel.phone ?? '';
    emailController.text = widget.userModel.email ?? '';
    jabatanController.text = widget.userModel.roleName ?? '';
    schoolController.text = widget.userModel.schoolName ?? '';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text('Profil Saya')),
        body: LoadingOverlay(
            isLoading: isLoadingPage,
            child: ListView(children: [
              ListTile(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (_) => ResetPasswordPage(
                                phone: phoneController.text,
                                email: emailController.text)));
                  },
                  title: const Text('Ubah Kata Sandi'),
                  trailing: const Icon(Icons.chevron_right)),
              Divider(color: greyColor, height: 0),
              Padding(
                  padding: EdgeInsets.all(defaultMargin),
                  child: Column(children: [
                    Container(
                        margin: const EdgeInsets.only(bottom: 16),
                        child: TextFieldBox(
                            controller: nipController,
                            title: 'NIP (Nomor Induk Pegawai)',
                            readOnly: true)),
                    Container(
                        margin: const EdgeInsets.only(bottom: 16),
                        child: TextFieldBox(
                            controller: nameController,
                            title: 'Nama Lengkap',
                            textCapitalization: TextCapitalization.words)),
                    Container(
                        margin: const EdgeInsets.only(bottom: 16),
                        child: TextFieldBox(
                            controller: phoneController,
                            title: 'Nomor Handphone',
                            keyboardType: TextInputType.number,
                            inputFormatter: [
                              FilteringTextInputFormatter.digitsOnly
                            ])),
                    Container(
                        margin: const EdgeInsets.only(bottom: 16),
                        child: TextFieldBox(
                            controller: emailController,
                            title: 'Alamat Email',
                            keyboardType: TextInputType.emailAddress)),
                    Container(
                        margin: const EdgeInsets.only(bottom: 16),
                        child: TextFieldBox(
                            controller: jabatanController,
                            title: 'Jabatan',
                            readOnly: true)),
                    Container(
                        margin: EdgeInsets.only(bottom: defaultMargin),
                        child: TextFieldBox(
                            controller: schoolController,
                            title: 'Tempat Bertugas',
                            readOnly: true)),
                    ButtonPrimary(
                        label: 'UBAH PROFIL',
                        onPressed: () {
                          update();
                        })
                  ]))
            ])));
  }
}
