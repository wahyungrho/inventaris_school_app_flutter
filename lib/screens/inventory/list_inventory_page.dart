part of '../../helpers/shared.dart';

class ListInventoryPage extends StatefulWidget {
  final String roleName;
  const ListInventoryPage({super.key, required this.roleName});

  @override
  State<ListInventoryPage> createState() => _ListInventoryPageState();
}

class _ListInventoryPageState extends State<ListInventoryPage> {
  List listCategories = [];
  String valCategory = "";

  Future<void> getCategories() async {
    GlobalModel response =
        await GlobalService.getData(BaseURL.categories, context);
    if (!mounted) return;
    if (response.status == 'success') {
      List data = [
        {'label': 'Semua Kategori', 'value': ''}
      ];
      for (dynamic item in response.result) {
        data.add({'label': item['name'], 'value': item['id']});
      }
      listCategories = data;
      setState(() {});
    } else {
      Helpers.showDialogAlert(alertWarning, response.errorMessage, context);
    }
  }

  Future<GlobalModel> getProduct() async {
    GlobalModel response =
        await GlobalService.getData('${BaseURL.products}$valCategory', context);
    return response;
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  void initState() {
    super.initState();
    getCategories();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text('Barang Inventaris')),
        body: Column(children: [
          Padding(
              padding: EdgeInsets.all(defaultMargin),
              child: SelectBox(
                  label: 'Kategori Barang',
                  valueItem: valCategory,
                  listItems: listCategories,
                  onChange: (val) {
                    valCategory = val;
                    setState(() {});
                  })),
          Divider(color: greyColor, height: 0),
          Expanded(
              child: FutureBuilder<GlobalModel>(
                  future: getProduct(),
                  builder: (context, snapshot) {
                    List<ProductModel> listProduct = [];
                    if (snapshot.connectionState == ConnectionState.done) {
                      if (snapshot.data?.status == 'success') {
                        for (var item in snapshot.data?.result) {
                          listProduct.add(ProductModel.fromJson(item));
                        }
                        if (listProduct.isNotEmpty) {
                          return ListView(
                              children: listProduct
                                  .map((e) => Column(children: [
                                        ListTile(
                                            onTap: () {
                                              if (widget.roleName == 'GURU') {
                                                Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (_) =>
                                                            DetailInventoryPage(
                                                                callback: () {
                                                                  setState(
                                                                      () {});
                                                                },
                                                                id: '${e.id}')));
                                              } else {
                                                Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: ((_) =>
                                                            FormInventoryPage(
                                                                method: () {
                                                                  setState(
                                                                      () {});
                                                                },
                                                                productModel:
                                                                    e))));
                                              }
                                            },
                                            leading: Container(
                                                height: 70,
                                                width: 50,
                                                decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            10),
                                                    boxShadow: [
                                                      BoxShadow(
                                                        color: greyColor ??
                                                            Colors.white,
                                                        offset: const Offset(
                                                            0.0, 0.0),
                                                        blurRadius: 0.5,
                                                        spreadRadius: 0.0,
                                                      ), //BoxShadow
                                                    ],
                                                    image: DecorationImage(
                                                        fit: BoxFit.cover,
                                                        image: NetworkImage(
                                                            '${BaseURL.apiAssets}/${e.image}')))),
                                            title: Text(e.name ?? ''),
                                            subtitle: Text(e.description ?? '',
                                                overflow:
                                                    TextOverflow.ellipsis),
                                            trailing:
                                                Text('${e.stock} Tersedia')),
                                        Divider(color: greyColor, height: 0)
                                      ]))
                                  .toList());
                        }
                        return Padding(
                          padding: EdgeInsets.only(top: defaultMargin),
                          child: const Text('Barang tidak ditemukan'),
                        );
                      }
                      return const SizedBox();
                    }
                    return const Center(child: CircularProgressIndicator());
                  }))
        ]),
        floatingActionButton: (widget.roleName == 'GURU')
            ? const SizedBox()
            : FloatingActionButton(
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: ((_) => FormInventoryPage(method: () {
                                setState(() {});
                              }))));
                },
                child: const Icon(Icons.add)));
  }
}
