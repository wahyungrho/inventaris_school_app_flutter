part of '../../helpers/shared.dart';

class FormInventoryPage extends StatefulWidget {
  final VoidCallback? method;
  final ProductModel? productModel;
  const FormInventoryPage({super.key, this.method, this.productModel});

  @override
  State<FormInventoryPage> createState() => _FormInventoryPageState();
}

class _FormInventoryPageState extends State<FormInventoryPage> {
  bool isLoadingPage = false;
  List listCategories = [];
  String valCategory = "";
  ImagePicker picker = ImagePicker();
  File? image;
  final TextEditingController codeProductController = TextEditingController();
  final TextEditingController nameProductController = TextEditingController();
  final TextEditingController descriptionProductController =
      TextEditingController();
  final TextEditingController stockProductController = TextEditingController();

  validation() {
    if (codeProductController.text.isEmpty ||
        nameProductController.text.isEmpty ||
        descriptionProductController.text.isEmpty ||
        stockProductController.text.isEmpty) {
      Helpers.showDialogAlert(
          alertWarning, "Mohon lengkapi form barang inventaris ...", context);
    } else if (widget.productModel == null && image == null) {
      Helpers.showDialogAlert(
          alertWarning, "Foto barang tidak boleh kosong ...", context);
    } else {
      submit();
    }
  }

  Future<void> getCategories() async {
    setState(() {
      isLoadingPage = true;
    });
    GlobalModel response =
        await GlobalService.getData(BaseURL.categories, context);
    if (!mounted) return;
    if (response.status == 'success') {
      List data = [
        {'label': 'Pilih kategori', 'value': ''}
      ];
      for (dynamic item in response.result) {
        data.add({'label': item['name'], 'value': item['id']});
      }
      listCategories = data;
      setState(() {});
    } else {
      Helpers.showDialogAlert(alertWarning, response.errorMessage, context);
    }
    setState(() {
      isLoadingPage = false;
    });
  }

  Future imageSource(String source) async {
    Navigator.pop(context);
    XFile? pickedFile;
    if (source == 'gallery') {
      pickedFile =
          await picker.pickImage(source: ImageSource.gallery, imageQuality: 25);
    } else {
      pickedFile =
          await picker.pickImage(source: ImageSource.camera, imageQuality: 25);
    }
    if (pickedFile != null) {
      // await uploadDiri(File(pickedFile.path));
      setState(() {
        image = File(pickedFile!.path);
      });
      debugPrint(image?.path);
    } else {
      if (!mounted) return;
      Helpers.showDialogAlert('Peringatan', 'No image selected.', context);

      debugPrint('No image selected.');
    }
  }

  Future<void> submit() async {
    try {
      Uri url = Uri.parse(BaseURL.addProduct);
      if (widget.productModel != null) {
        url = Uri.parse(BaseURL.updateProduct);
      }
      setState(() {
        isLoadingPage = true;
      });
      http.MultipartRequest request = http.MultipartRequest('POST', url);
      if (widget.productModel != null) {
        request.fields['id'] = widget.productModel?.id ?? '';
      }
      request.fields['categoryID'] = valCategory;
      request.fields['codeProduct'] =
          Helpers.removeSpecialChar(codeProductController.text);
      request.fields['name'] =
          Helpers.removeSpecialChar(nameProductController.text);
      request.fields['description'] =
          Helpers.removeSpecialChar(descriptionProductController.text);
      request.fields['stock'] =
          Helpers.removeSpecialChar(stockProductController.text);
      if (image != null) {
        request.files
            .add(await http.MultipartFile.fromPath('image', image!.path));
      } else {
        request.fields['image'] = widget.productModel!.image!;
      }
      request.send().then((value) async {
        http.Response.fromStream(value).then((response) {
          GlobalModel data = GlobalModel.fromJson(jsonDecode(response.body));
          if (data.status == 'success') {
            Helpers.showDialogAlertFunction(alertSuccess, data.result, () {
              widget.method!();
              Navigator.pop(context);
              Navigator.pop(context);
            }, context);
          } else {
            Helpers.showDialogAlert(alertWarning, data.errorMessage, context);
          }
        });
      });
      setState(() {
        isLoadingPage = false;
      });
    } catch (e) {
      debugPrint('Error $e');
      setState(() {
        isLoadingPage = false;
      });
    }
  }

  Future<void> delete() async {
    setState(() {
      isLoadingPage = true;
    });
    GlobalModel response = await GlobalService.postData(
        BaseURL.deleteProduct, {'id': widget.productModel?.id}, context);
    if (!mounted) return;
    if (response.status == 'success') {
      Helpers.showDialogAlertFunction(alertSuccess, response.result, () {
        widget.method!();
        Navigator.pop(context);
        Navigator.pop(context);
      }, context);
    } else {
      Helpers.showDialogAlert(alertWarning, response.errorMessage, context);
    }
    setState(() {
      isLoadingPage = false;
    });
  }

  void initialLoaded() {
    getCategories();
    if (widget.productModel != null) {
      valCategory = widget.productModel?.categoryID ?? '';
      codeProductController.text = widget.productModel?.codeProduct ?? '';
      nameProductController.text = widget.productModel?.name ?? '';
      descriptionProductController.text =
          widget.productModel?.description ?? '';
      stockProductController.text = widget.productModel?.stock ?? '';
      setState(() {});
    }
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  void initState() {
    super.initState();
    initialLoaded();
  }

  @override
  Widget build(BuildContext context) {
    void modalBottomUpload() => showModalBottomSheet(
        context: context,
        builder: (context) {
          return Padding(
              padding: const EdgeInsets.symmetric(vertical: 15),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 15),
                        child:
                            Text("Pilih sumber foto", style: mediumFontStyle)),
                    const SizedBox(height: 8),
                    Wrap(children: [
                      ListTile(
                          onTap: () {
                            imageSource('camera');
                          },
                          leading: Icon(Icons.camera_alt_outlined,
                              size: defaultMargin),
                          title: const Text('Kamera')),
                      ListTile(
                          onTap: () {
                            imageSource('gallery');
                          },
                          leading: Icon(Icons.photo_library_outlined,
                              size: defaultMargin),
                          title: const Text('Galeri'))
                    ])
                  ]));
        });

    Widget fotoBarangWidget() => Container(
        margin: const EdgeInsets.only(bottom: 15),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Text('Foto Barang', style: mediumFontStyle),
          const SizedBox(height: 8),
          InkWell(
              borderRadius: BorderRadius.circular(5),
              onTap: () {
                modalBottomUpload();
              },
              child: Container(
                  height: 150,
                  width: double.infinity,
                  decoration: BoxDecoration(
                      border: Border.all(width: 1),
                      borderRadius: BorderRadius.circular(5)),
                  child: (widget.productModel != null && image == null)
                      ? ClipRRect(
                          borderRadius: BorderRadius.circular(5),
                          child: Image.network(
                              "${BaseURL.apiAssets}/${widget.productModel?.image}",
                              fit: BoxFit.cover, frameBuilder:
                                  (BuildContext context, Widget child,
                                      int? frame, bool wasSynchronouslyLoaded) {
                            if (wasSynchronouslyLoaded) {
                              return child;
                            }
                            return AnimatedOpacity(
                                opacity: frame == null ? 0 : 1,
                                duration: const Duration(milliseconds: 2000),
                                curve: Curves.easeOut,
                                child: child);
                          }))
                      : (image == null)
                          ? Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                  Icon(Icons.upload_outlined,
                                      size: 30, color: greyColor),
                                  const SizedBox(height: 8),
                                  Text('Unggah Foto Barang Inventaris',
                                      style: regulerFontStyle.copyWith(
                                          color: greyColor))
                                ])
                          : ClipRRect(
                              borderRadius: BorderRadius.circular(5),
                              child: Image.file(image!, fit: BoxFit.cover,
                                  frameBuilder: (BuildContext context,
                                      Widget child,
                                      int? frame,
                                      bool wasSynchronouslyLoaded) {
                                if (wasSynchronouslyLoaded) {
                                  return child;
                                }
                                return AnimatedOpacity(
                                    opacity: frame == null ? 0 : 1,
                                    duration:
                                        const Duration(milliseconds: 2000),
                                    curve: Curves.easeOut,
                                    child: child);
                              }))))
        ]));

    Widget categoryWidget() => Container(
        margin: const EdgeInsets.only(bottom: 15),
        child: SelectBox(
            label: 'Kategori Barang',
            valueItem: valCategory,
            listItems: listCategories,
            onChange: (val) {
              valCategory = val;
              setState(() {});
            }));

    Widget codeProductWidget() => Container(
        margin: const EdgeInsets.only(bottom: 15),
        child: TextFieldBox(
            controller: codeProductController,
            title: 'Kode Barang',
            textCapitalization: TextCapitalization.characters,
            hintText: 'Masukkan kode barang'));

    Widget nameProductWidget() => Container(
        margin: const EdgeInsets.only(bottom: 15),
        child: TextFieldBox(
            controller: nameProductController,
            title: 'Nama Barang',
            textCapitalization: TextCapitalization.words,
            hintText: 'Masukkan nama barang'));

    Widget descriptionProductWidget() => Container(
        margin: const EdgeInsets.only(bottom: 15),
        child: TextFieldBox(
            controller: descriptionProductController,
            title: 'Deskripsi Barang',
            maxLines: null,
            keyboardType: TextInputType.multiline,
            textCapitalization: TextCapitalization.sentences,
            hintText: 'Masukkan deskripsi barang'));

    Widget stockProductWidget() => Container(
        margin: const EdgeInsets.only(bottom: 15),
        child: TextFieldBox(
            controller: stockProductController,
            title: 'Stok Barang',
            maxLines: null,
            keyboardType: TextInputType.number,
            inputFormatter: [FilteringTextInputFormatter.digitsOnly],
            hintText: 'Masukkan stok barang'));

    Widget buttonAction() => Container(
        margin: const EdgeInsets.only(top: 10),
        child: ButtonPrimary(
            label: 'SIMPAN',
            onPressed: () {
              validation();
            }));

    Widget buttonDelete() => Container(
        margin: const EdgeInsets.only(top: 10),
        child: ButtonOutline(
            label: 'HAPUS DATA',
            onPressed: () {
              Helpers.showDialogConfirmationFunction(
                  alertWarning, 'Apakah kamu yakin menghapus data ini ?', () {
                delete();
              },  context);
            }));

    return Scaffold(
        appBar: AppBar(
            title: Text(
                '${(widget.productModel != null) ? 'Ubah' : 'Tambah'} Barang Inventaris')),
        body: LoadingOverlay(
            isLoading: isLoadingPage,
            child: ListView(padding: EdgeInsets.all(defaultMargin), children: [
              fotoBarangWidget(),
              categoryWidget(),
              codeProductWidget(),
              nameProductWidget(),
              descriptionProductWidget(),
              stockProductWidget(),
              buttonAction(),
         if(widget.productModel != null)     buttonDelete()
            ])));
  }
}
