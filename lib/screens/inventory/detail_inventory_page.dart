part of '../../helpers/shared.dart';

class DetailInventoryPage extends StatelessWidget {
  final String id;
  final VoidCallback? callback;
  final bool? isFromReport;
  const DetailInventoryPage(
      {super.key, required this.id, this.callback, this.isFromReport = false});

  @override
  Widget build(BuildContext context) {
    Future<ProductModel?> detailInventory() async {
      GlobalModel response =
          await GlobalService.getData('${BaseURL.detailProduct}$id', context);
      if (response.status == 'success') {
        return ProductModel.fromJson(response.result);
      }
      return null;
    }

    return Scaffold(
        appBar: AppBar(title: const Text('Detail Barang Inventaris')),
        body: FutureBuilder<ProductModel?>(
            future: detailInventory(),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.done) {
                if (snapshot.hasData || snapshot.data != null) {
                  final data = snapshot.data;
                  return Column(children: [
                    Expanded(
                        child: ListView(children: [
                      Container(
                          height: 230,
                          margin: const EdgeInsets.only(bottom: 16),
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  fit: BoxFit.fill,
                                  image: NetworkImage(
                                      '${BaseURL.apiAssets}/${data?.image}')))),
                      Padding(
                          padding:
                              EdgeInsets.symmetric(horizontal: defaultMargin),
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('${data?.codeProduct} - ${data?.name}',
                                    style:
                                        boldFontStyle.copyWith(fontSize: 18)),
                                const SizedBox(height: 2),
                                Text(data?.categoryName ?? '',
                                    style:
                                        lightFontStyle.copyWith(fontSize: 15)),
                              ])),
                      const SizedBox(height: 16),
                      Divider(color: greyColor, height: 0),
                      const SizedBox(height: 16),
                      Padding(
                          padding:
                              EdgeInsets.symmetric(horizontal: defaultMargin),
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('Deskripsi',
                                    style:
                                        mediumFontStyle.copyWith(fontSize: 15)),
                                const SizedBox(height: 5),
                                Text(data?.description ?? '',
                                    style: regulerFontStyle.copyWith(
                                        fontSize: 15)),
                              ]))
                    ])),
                    if (isFromReport == false)
                      Padding(
                          padding: const EdgeInsets.all(16),
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(children: [
                                  Text('Tersedia : ',
                                      style: lightFontStyle.copyWith(
                                          fontSize: 15)),
                                  Text('${data?.stock} ',
                                      style:
                                          boldFontStyle.copyWith(fontSize: 15)),
                                  Text('Barang',
                                      style: lightFontStyle.copyWith(
                                          fontSize: 15)),
                                ]),
                                const SizedBox(height: 10),
                                ButtonPrimary(
                                    label: 'AJUKAN PEMINJAMAN',
                                    onPressed: () {
                                      Navigator.pushReplacement(
                                          context,
                                          MaterialPageRoute(
                                              builder: (_) =>
                                                  PengajuanInventoryPage(
                                                      method: callback,
                                                      productModel: data!)));
                                    })
                              ]))
                  ]);
                }
                return const Center(child: Text('Barang tidak ditemukan'));
              }
              return const Center(child: CircularProgressIndicator());
            }));
  }
}
