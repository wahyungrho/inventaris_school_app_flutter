part of '../../helpers/shared.dart';

class PengajuanInventoryPage extends StatefulWidget {
  final ProductModel productModel;
  final VoidCallback? method;
  const PengajuanInventoryPage(
      {super.key, required this.productModel, this.method});

  @override
  State<PengajuanInventoryPage> createState() => _PengajuanInventoryPageState();
}

class _PengajuanInventoryPageState extends State<PengajuanInventoryPage> {
  bool isLoading = false;
  UserModel userModel = UserModel();
  final TextEditingController quantityController = TextEditingController();
  final TextEditingController notesController = TextEditingController();

  Future<void> userdetail() async {
    SharedPreferences preferences = await GlobalService.sharedPreference;
    String? userID = preferences.getString(PreferenceModel.userID) ?? '';
    setState(() {
      isLoading = true;
    });
    if (!mounted) return;
    GlobalModel response =
        await GlobalService.getData('${BaseURL.userDetail}$userID', context);
    if (response.status == 'success') {
      userModel = UserModel.fromJson(response.result);
      setState(() {});
    } else {
      if (!mounted) return;
      Helpers.showDialogAlert(alertWarning, response.errorMessage, context);
    }
    setState(() {
      isLoading = false;
    });
  }

  Future<void> loanInventory() async {
    setState(() {
      isLoading = true;
    });
    GlobalModel response = await GlobalService.postData(
        BaseURL.loanInventaris,
        {
          'user_id': userModel.id,
          'product_id': widget.productModel.id,
          'quantity': quantityController.text,
          'notes': notesController.text
        },
        context);
    if (!mounted) return;
    if (response.status == 'success') {
      widget.method!();
      Helpers.showDialogAlertFunction(alertSuccess, response.result, () {
        Navigator.pop(context);
        Navigator.pop(context);
      }, context);
    } else {
      Helpers.showDialogAlert(alertWarning, response.errorMessage, context);
    }
    setState(() {
      isLoading = false;
    });
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  void dispose() {
    quantityController.dispose();
    notesController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    userdetail();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text('Peminjaman Barang Inventaris')),
        body: LoadingOverlay(
            isLoading: isLoading,
            child: ListView(children: [
              ListTile(
                  leading: Container(
                      height: 70,
                      width: 50,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          boxShadow: [
                            BoxShadow(
                              color: greyColor ?? Colors.white,
                              offset: const Offset(0.0, 0.0),
                              blurRadius: 0.5,
                              spreadRadius: 0.0,
                            ), //BoxShadow
                          ],
                          image: DecorationImage(
                              fit: BoxFit.cover,
                              image: NetworkImage(
                                  '${BaseURL.apiAssets}/${widget.productModel.image}')))),
                  title: Text(
                      '${widget.productModel.codeProduct} - ${widget.productModel.name}'),
                  subtitle: Text(widget.productModel.description ?? '',
                      overflow: TextOverflow.ellipsis)),
              Divider(color: greyColor, height: 0),
              Padding(
                  padding: EdgeInsets.all(defaultMargin),
                  child: Column(children: [
                    Container(
                        margin: const EdgeInsets.only(bottom: 16),
                        child: TextFieldBox(
                            controller: quantityController,
                            title: 'Jumlah Barang Dipinjam',
                            hintText: 'Masukkan jumlah barang dipinjam',
                            keyboardType: TextInputType.number,
                            inputFormatter: [
                              FilteringTextInputFormatter.digitsOnly,
                              LengthLimitingTextInputFormatter(5)
                            ])),
                    Container(
                        margin: EdgeInsets.only(bottom: defaultMargin),
                        child: TextFieldBox(
                            controller: notesController,
                            title: 'Keterangan (Opsional)',
                            hintText: 'Masukkan keterangan',
                            keyboardType: TextInputType.multiline,
                            maxLines: null,
                            textCapitalization: TextCapitalization.sentences)),
                    ButtonPrimary(
                        label: 'AJUKAN PEMINJAMAN BARANG',
                        onPressed: () {
                          loanInventory();
                        })
                  ]))
            ])));
  }
}
